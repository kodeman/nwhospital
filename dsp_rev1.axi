PROGRAM_NAME='dsp_rev1'
//=========================================================//
//=========================================================//
DEFINE_CONSTANT
//---------------------------------------------------------//


//=========================================================//
//=========================================================//
DEFINE_TYPE
//---------------------------------------------------------//


//=========================================================//
//=========================================================//
DEFINE_VARIABLE
//---------------------------------------------------------//
char dsp_vol_instance[NUM_VOL][3];
char dsp_vol_type[NUM_VOL][20];
char dsp_mute_type[NUM_VOL][20];
char dsp_vol_mute_chan[NUM_VOL][3];
char dsp_vol_mute_direction[NUM_VOL][3];
char dsp_vol_mute_mixer_type[NUM_VOL][3];

char DSP_FADER_VOL[20]     = 'FADER=';
char DSP_FADER_MUTE[20]    = 'FADER_MUTE=';
char DSP_MIXER_VOL[20]     = 'MIXER_LEVEL=';
char DSP_MIXER_MUTE[20]    = 'MIXER_MUTE=';
char DSP_MIXER_INPUT[3]    = 'I';
char DSP_MIXER_OUTPUT[3]   = 'O';
char	DSP_AUTO_MIXER[3]     = 'A';
char	DSP_MATRIX_MIXER[3]   = 'M';
char	DSP_STANDARD_MIXER[3] = 'S';
// instance ID's of the 3 "program volume" faders on input side of mixer...
char DSP_FADER_INST_CODEC[3] = '33';
char DSP_FADER_INST_VR1[3]   = '34';
char DSP_FADER_INST_VR2[3]   = '35';

integer dsp_debug = 0;
//=========================================================//
//=========================================================//
// ctrl = VOL_PROG, VOL_SPEECH, VOL_POD_MIC, VOL_WMIC
// action = cmdVOL_UP, cmdVOL_DOWN, cmdMUTE_TOGGLE, cmdMUTE_ON, cmdMUTE_OFF
define_function do_dsp_vol_mute(integer ctrl, char action[])
{
	local_var char strCmd[15];
	local_var char strInstance[3];
	local_var char strChannel[3];
	local_var char strValue[3];
	local_var char strDirection[3];
	local_var char strType[3];
	
	strInstance  = dsp_vol_instance[ctrl];
	strChannel   = dsp_vol_mute_chan[ctrl];
	strDirection = dsp_vol_mute_direction[ctrl];
	strType      = dsp_vol_mute_mixer_type[ctrl];
	
	select
	{
	 active(find_string(action, cmdVOL_UP, 1)):
		{
		 strCmd = dsp_vol_type[ctrl];
			if(ctrl == VOL_PROG) 
			{
    if(vol_level_fb[VOL_PROG] + 2 < 100) { vol_level_fb[VOL_PROG] = vol_level_fb[VOL_PROG] + 2 }
				strValue = itoa(vol_level_fb[VOL_PROG]);
			}
			else { strValue = "'+'"; }
		}
		active(find_string(action, cmdVOL_DOWN, 1)):
		{
		 strCmd = dsp_vol_type[ctrl];
			if(ctrl == VOL_PROG) 
			{
    if(vol_level_fb[VOL_PROG] - 2 > 0) { vol_level_fb[VOL_PROG] = vol_level_fb[VOL_PROG] - 2 }
				strValue = itoa(vol_level_fb[VOL_PROG]);
			}
			else { strValue = "'-'"; }
		}
		active(find_string(action, cmdMUTE_TOGGLE, 1)):
		{
			strCmd = dsp_mute_type[ctrl];
			if(mute_fb[ctrl] == 1) // it's muted so send un-mute command...
			{
 			strValue = "'0'";
			}
			else // un-muted so send mute command...
			{
			 strValue = "'1'";
			}
		}
		active(find_string(action, cmdMUTE_ON, 1)):
		{
			strCmd = dsp_mute_type[ctrl];
			strValue = "'1'";
		}
		active(find_string(action, cmdMUTE_OFF, 1)):
		{
			strCmd = dsp_mute_type[ctrl];
			strValue = "'0'";
		}
	 active(1): 
		{
		 strCmd = "";
			strValue = "";
  }
	}
	
	if(length_string(strCmd) && length_string(strInstance) && length_string(strChannel) && length_string(strValue))
	{
	 if(find_string(strCmd, "DSP_FADER_VOL", 1) || find_string(strCmd, "DSP_FADER_MUTE", 1))
		{
		 // format string for fader...
			// FADER INFO: ////////////////////
	  // FADER LEVEL:
	  // string format:  "'FADER=<instance>:<channel>:<value>'"
	  // <instance>:     instance ID of control.
	  // <channel>:      channel of fader (always 1).
	  // <value>:        +, -, or 0..100.
	  // FADER MUTE:
	  // string format:  "'FADER_MUTE=<instance>:<channel>:<value>'"
	  // <instance>:     instance ID of control.
	  // <channel>:      channel of fader (always 1).
	  // <value>:        0 = mute off, 1 = mute on.
			///////////////////////////////////
			if(find_string(strInstance, "dsp_vol_instance[VOL_PROG]",1)) // if this is "program" volume...
			{
			 send_command vdvDSP,"strCmd,DSP_FADER_INST_CODEC,':',strChannel,':',strValue";
				send_command vdvDSP,"strCmd,DSP_FADER_INST_VR1,':',strChannel,':',strValue";
				send_command vdvDSP,"strCmd,DSP_FADER_INST_VR2,':',strChannel,':',strValue";
			}
			else // else it's speech volume...
			{
			 send_command vdvDSP,"strCmd,strInstance,':',strChannel,':',strValue";
			}
		}
		else
		{
		 // format string for mixer...
			// MIXER INFO: ////////////////////
	  // MIXER LEVEL:
	  // string format:  "'MIXER_MUTE=<instance>:<direction>:<type>:<value>:<channel>'"
	  // <instance>:     instance ID of control.
	  // <direction>:    I = input, O = output.
	  // <type>:         A = automixer, M = matrix mixer, S = standard mixer.
	  // <value>:        +, -, or 0..100.
	  // <channel>:      channel of mixer.
	  // MIXER MUTE:
	  // string format:  "'MIXER_MUTE=<instance>:<direction>:<type>:<value>:<channel>'"
	  // <instance>:     instance ID of control.
	  // <direction>:    I = input, O = output.
	  // <type>:         A = automixer, M = matrix mixer, S = standard mixer.
	  // <value>:        0 = mute off, 1 = mute on.
	  // <channel>:      channel of mixer.
			///////////////////////////////////
			send_command vdvDSP,"strCmd,strInstance,':',strDirection,':',strType,':',strValue,':',strChannel";
		}
	}
}
//-----------------------------------------------//
define_function do_vid_conf_mixer_mute(integer new_mute_state)
{
 // format string for mixer...
	// MIXER INFO: ////////////////////
	// MIXER MUTE:
	// string format:  "'MIXER_MUTE=<instance>:<direction>:<type>:<value>:<channel>'"
	// <instance>:     instance ID of control.
	// <direction>:    I = input, O = output.
	// <type>:         A = automixer, M = matrix mixer, S = standard mixer.
	// <value>:        0 = mute off, 1 = mute on.
	// <channel>:      channel of mixer.
	///////////////////////////////////
 vid_conf_mute = new_mute_state;
 switch(new_mute_state)
	{
	 case 0: // unmute
		{
		 send_command vdvDSP,"'MIXER_MUTE=1:I:M:0:1'";
			send_command vdvDSP,"'MIXER_MUTE=1:I:M:0:2'";
		}
		case 1: // mute
		{
		 send_command vdvDSP,"'MIXER_MUTE=1:I:M:1:1'";
			send_command vdvDSP,"'MIXER_MUTE=1:I:M:1:2'";
		}
		default: {  }
	}
}
//=========================================================//
//=========================================================//
DEFINE_START
//---------------------------------------------------------//
vol_level_fb[VOL_PROG] = 70;

dsp_vol_instance[VOL_PROG]           = "DSP_FADER_INST_CODEC";
dsp_vol_type[VOL_PROG]               = "DSP_FADER_VOL";
dsp_mute_type[VOL_PROG]              = "DSP_FADER_MUTE";
dsp_vol_mute_chan[VOL_PROG]          = "'1'";
dsp_vol_mute_direction[VOL_PROG]     = "";
dsp_vol_mute_mixer_type[VOL_PROG]    = "";
//dsp_vol_instance[VOL_PROG]           = "'36'";
//dsp_vol_type[VOL_PROG]               = "DSP_FADER_VOL";
//dsp_mute_type[VOL_PROG]              = "DSP_FADER_MUTE";
//dsp_vol_mute_chan[VOL_PROG]          = "'1'";
//dsp_vol_mute_direction[VOL_PROG]     = "";
//dsp_vol_mute_mixer_type[VOL_PROG]    = "";
dsp_vol_instance[VOL_SPEECH]         = "'27'";
dsp_vol_type[VOL_SPEECH]             = "DSP_FADER_VOL";
dsp_mute_type[VOL_SPEECH]            = "DSP_FADER_MUTE";
dsp_vol_mute_chan[VOL_SPEECH]        = "'1'";
dsp_vol_mute_direction[VOL_SPEECH]   = "";
dsp_vol_mute_mixer_type[VOL_SPEECH]  = "";
dsp_vol_instance[VOL_POD_MIC]        = "'1'";
dsp_vol_type[VOL_POD_MIC]            = "DSP_MIXER_VOL";
dsp_mute_type[VOL_POD_MIC]           = "DSP_MIXER_MUTE";
dsp_vol_mute_chan[VOL_POD_MIC]       = "'12'";
dsp_vol_mute_direction[VOL_POD_MIC]  = "DSP_MIXER_INPUT";
dsp_vol_mute_mixer_type[VOL_POD_MIC] = "DSP_MATRIX_MIXER";
dsp_vol_instance[VOL_WMIC]           = "'1'";
dsp_vol_type[VOL_WMIC]               = "DSP_MIXER_VOL";
dsp_mute_type[VOL_WMIC]              = "DSP_MIXER_MUTE";
dsp_vol_mute_chan[VOL_WMIC]          = "'7'";
dsp_vol_mute_direction[VOL_WMIC]     = "DSP_MIXER_INPUT";
dsp_vol_mute_mixer_type[VOL_WMIC]    = "DSP_MATRIX_MIXER";
//=========================================================//
//=========================================================//
DEFINE_EVENT
//---------------------------------------------------------//
//-----------------------------------------------//
data_event[vdvDSP] // virtual device
{
 online:
	{
	 send_command data.device,"'INCREMENT_SET=FADR:2'"; // set increment steps for fader level...
		send_command data.device,"'INCREMENT_SET=MXRL:2'"; // set increment steps for matrix mixer level...
		// set vol level of 3 program faders...
		send_command data.device,"DSP_FADER_VOL,DSP_FADER_INST_CODEC,dsp_vol_mute_chan[VOL_PROG],
				itoa(vol_level_fb[VOL_PROG])";
		send_command data.device,"DSP_FADER_VOL,DSP_FADER_INST_VR1,dsp_vol_mute_chan[VOL_PROG],
				itoa(vol_level_fb[VOL_PROG])";
		send_command data.device,"DSP_FADER_VOL,DSP_FADER_INST_VR2,dsp_vol_mute_chan[VOL_PROG],
				itoa(vol_level_fb[VOL_PROG])";
		// get vol levels...
	 send_command data.device,"'FADER?',dsp_vol_instance[VOL_PROG],':',dsp_vol_mute_chan[VOL_PROG]";      // vol level status for program...
		send_command data.device,"'FADER_MUTE?',dsp_vol_instance[VOL_PROG],':',dsp_vol_mute_chan[VOL_PROG]"; // mute status for program...
		send_command data.device,"'FADER?',dsp_vol_instance[VOL_SPEECH],':',dsp_vol_mute_chan[VOL_SPEECH]";      // vol level status for speech...
		send_command data.device,"'FADER_MUTE?',dsp_vol_instance[VOL_SPEECH],':',dsp_vol_mute_chan[VOL_SPEECH]"; // mute status for speech...
		send_command data.device,"'MIXER_LEVEL?',dsp_vol_instance[VOL_POD_MIC],':',       // vol level status for podium mic...
		                                         dsp_vol_mute_direction[VOL_POD_MIC],':',
																																											dsp_vol_type[VOL_POD_MIC],':',
																																											dsp_vol_mute_chan[VOL_POD_MIC]";
  send_command data.device,"'MIXER_MUTE?',dsp_vol_instance[VOL_POD_MIC],':',       // mute status for podium mic...
		                                        dsp_vol_mute_direction[VOL_POD_MIC],':',
																																										dsp_vol_type[VOL_POD_MIC],':',
																																										dsp_vol_mute_chan[VOL_POD_MIC]";
		send_command data.device,"'MIXER_LEVEL?',dsp_vol_instance[VOL_WMIC],':',       // vol level status for wireless mic...
		                                         dsp_vol_mute_direction[VOL_WMIC],':',
																																											dsp_vol_type[VOL_WMIC],':',
																																											dsp_vol_mute_chan[VOL_WMIC]";
  send_command data.device,"'MIXER_MUTE?',dsp_vol_instance[VOL_WMIC],':',       // mute status for wireless mic...
		                                        dsp_vol_mute_direction[VOL_WMIC],':',
																																										dsp_vol_type[VOL_WMIC],':',
																																										dsp_vol_mute_chan[VOL_WMIC]";
	}
	string: // process data returned from module...
	{
	 stack_var char rtn_msg[40];
		stack_var char junk[10];
		rtn_msg = data.text;
		
		select
		{
		 ////----- fader volume feedback -----////
		 active(find_string(rtn_msg, "DSP_FADER_VOL", 1)):
			{
			 // string format:  "'FADER=<instance>:<channel>:<value>'"
				local_var integer i;
				local_var char strInstance[3];
				local_var char strChannel[3];
				local_var char strValue[3];
				
				junk = remove_string(rtn_msg, "DSP_FADER_VOL", 1);
				strInstance = left_string(rtn_msg, ( find_string(rtn_msg, "':'", 1) - 1 ));
				junk = remove_string(rtn_msg, "':'", 1);
				strChannel = left_string(rtn_msg, ( find_string(rtn_msg, "':'", 1) - 1 ));
				junk = remove_string(rtn_msg, "':'", 1);
				strValue = rtn_msg;
				
				for(i = 1; i <= NUM_VOL; i++)
				{
				 if(strInstance == dsp_vol_instance[i])
					{
					 vol_level_fb[i] = atoi(strValue);
					}
				}
				update_tp_vol_levels();
			}
			////----- fader mute feedback -----////
			active(find_string(rtn_msg, "DSP_FADER_MUTE", 1)):
			{
			 // string format:  "'FADER_MUTE=<instance>:<channel>:<state>'"
				local_var integer i;
				local_var char strInstance[3];
				local_var char strChannel[3];
				local_var char strState[3];
				
				junk = remove_string(rtn_msg, "DSP_FADER_MUTE", 1);
				strInstance = left_string(rtn_msg, ( find_string(rtn_msg, "':'", 1) - 1 ));
				junk = remove_string(rtn_msg, "':'", 1);
				strChannel = left_string(rtn_msg, ( find_string(rtn_msg, "':'", 1) - 1 ));
				junk = remove_string(rtn_msg, "':'", 1);
				strState = rtn_msg;
				
				for(i = 1; i <= NUM_VOL; i++)
				{
				 if(strInstance == dsp_vol_instance[i])
					{
					 mute_fb[i] = atoi(strState);
					}
				}
			}
			////----- mixer volume feedback -----////
			active(find_string(rtn_msg, "DSP_MIXER_VOL", 1)):
			{
			 // string format:  "'MIXER_LEVEL=<instance>:<directrion>:<mixer_type>:<value>:<channel>'"
				local_var integer i;
				local_var char strInstance[3];
				local_var char strChannel[3];
				local_var char strValue[3];
				local_var char strDirection[3];
	   local_var char strType[3];
				
				junk = remove_string(rtn_msg, "DSP_MIXER_VOL", 1);                                // remove beginning of message...
				strInstance = left_string(rtn_msg, ( find_string(rtn_msg, "':'", 1) - 1 ));       // get instance...
				if(dsp_debug) { send_string 0,"__file__,' strInstance == ',strInstance,'!'"; }    // debug...
				junk = remove_string(rtn_msg, "':'", 1);                                          // get rid of "':'"...
				strDirection = left_string(rtn_msg, ( find_string(rtn_msg, "':'", 1) - 1 ));      // get direction ("I"nput or "O"utput)...
				if(dsp_debug) { send_string 0,"__file__,' strDirection == ',strDirection,'!'"; }  // debug...
				junk = remove_string(rtn_msg, "':'", 1);                                          // get rid of "':'"...
				strType = left_string(rtn_msg, ( find_string(rtn_msg, "':'", 1) - 1 ));           // get mixer type ("M"atrix, "A"uto, or "S"tandard)...
				if(dsp_debug) { send_string 0,"__file__,' strType == ',strType,'!'"; }            // debug...
				junk = remove_string(rtn_msg, "':'", 1);                                          // get rid of "':'"...
				strValue = left_string(rtn_msg, ( find_string(rtn_msg, "':'", 1) - 1 ));          // get value (0-100)...
				if(dsp_debug) { send_string 0,"__file__,' strValue == ',strValue,'!'"; }          // debug...
				junk = remove_string(rtn_msg, "':'", 1);                                          // get rid of "':'"...
				strChannel = rtn_msg;                                                             // get channel (channel number of mixer)...
				if(dsp_debug) { send_string 0,"__file__,' strChannel == ',strChannel,'!'"; }      // debug...
				
				for(i = 1; i <= NUM_VOL; i++)
				{
				 if( (strInstance == dsp_vol_instance[i]) && (strChannel == dsp_vol_mute_chan[i]) )
					{
					 vol_level_fb[i] = atoi(strValue);
					}
				}
				update_tp_vol_levels();
			}
			////----- mixer mute feedback -----////
			active( find_string(rtn_msg, "DSP_MIXER_MUTE", 1) ):
			{
			 // string format:  "'MIXER_MUTE=<instance>:<directrion>:<mixer_type>:<value>:<channel>'"
				local_var integer i;
				local_var char strInstance[3];
				local_var char strChannel[3];
				local_var char strValue[3];
				local_var char strDirection[3];
	   local_var char strType[3];
				
				junk = remove_string(rtn_msg, "DSP_MIXER_MUTE", 1);                                 // remove beginning of message...
				strInstance = left_string(rtn_msg, ( find_string(rtn_msg, "':'", 1) - 1 ));        // get instance...
				if(dsp_debug) { send_string 0,"__file__,' strInstance == ',strInstance,'!'"; }     // debug...
				junk = remove_string(rtn_msg, "':'", 1);                                           // get rid of "':'"...
				strDirection = left_string(rtn_msg, ( find_string(rtn_msg, "':'", 1) - 1 ));       // get direction ("I"nput or "O"utput)...
				if(dsp_debug) { send_string 0,"__file__,' strDirection == ',strDirection,'!'"; }   // debug...
				junk = remove_string(rtn_msg, "':'", 1);                                           // get rid of "':'"...
				strType = left_string(rtn_msg, ( find_string(rtn_msg, "':'", 1) - 1 ));            // get mixer type ("M"atrix, "A"uto, or "S"tandard)...
				if(dsp_debug) { send_string 0,"__file__,' strType == ',strType,'!'"; }             // debug...
				junk = remove_string(rtn_msg, "':'", 1);                                           // get rid of "':'"...
				strValue = left_string(rtn_msg, ( find_string(rtn_msg, "':'", 1) - 1 ));           // get value (0-100)...
				if(dsp_debug) { send_string 0,"__file__,' strValue == ',strValue,'!'"; }           // debug...
				junk = remove_string(rtn_msg, "':'", 1);                                           // get rid of "':'"...
				strChannel = rtn_msg;                                                              // get channel (channel number of mixer)...
				if(dsp_debug) { send_string 0,"__file__,' strChannel == ',strChannel,'!'"; }       // debug...
				
				for(i = 1; i <= NUM_VOL; i++)
				{
				 if( (strInstance == dsp_vol_instance[i]) && (strChannel == dsp_vol_mute_chan[i]) )
					{
					 mute_fb[i] = atoi(strValue);
					}
				}
			}
		 active(1): {  }
		}
	}
}


//=========================================================//
//=========================================================//
DEFINE_PROGRAM
//---------------------------------------------------------//


//=========================================================//
//                     END OF PROGRAM                      //
//        DO NOT PUT ANY CODE BELOW THIS COMMENT           //
//=========================================================//
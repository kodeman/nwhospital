PROGRAM_NAME='Keypads'

DEFINE_CONSTANT

lecturn 	= 1
blu_ray 	= 2
conf		= 3
dvd		= 4
table		= 5
sat		= 6

navup		= 7
navdn		= 8
navleft 	= 9
navright 	= 10
navselect 	= 11

volup		= 12
voldn		= 13

lightson 	= 1
lightspresent	= 2
lightsdvd	= 3
lightswork	= 4
lightsoff	= 5
shadesopen	= 6
shadesclose	= 7

DEFINE_TYPE
//---------------------------------------------------------//


//=========================================================//
//=========================================================//
DEFINE_VARIABLE

integer nActivePodium
integer nActiveTable

DEFINE_START

DEFINE_EVENT


BUTTON_EVENT[dvMainKey,lecturn]{
    PUSH:{
	switch(tp_last_source_selected){
	    CASE btnSRC_PODIUM_PC:{
		DO_PUSH(dvTP,btnSRC_PODIUM_LAPTOP_DVI)
	    }
	    CASE btnSRC_PODIUM_LAPTOP_DVI:{
		DO_PUSH(dvTP,btnSRC_PODIUM_LAPTOP_RGB)
	    }
	    CASE btnSRC_PODIUM_LAPTOP_RGB:{
		DO_PUSH(dvTP,btnSRC_PODIUM_PC)
	    }
	    DEFAULT:{
		DO_PUSH(dvTP,btnSRC_PODIUM_PC)
	    }
	}
	
    }
    HOLD[30]:{
	DO_PUSH(dvTP,btnSRC_SYSTEM_OFF)
    }
}

BUTTON_EVENT[dvMainKey,blu_ray]{
    PUSH:{
	DO_PUSH(dvTP,btnSRC_HDDVD)
    }
    HOLD[30]:{
	DO_PUSH(dvTP,btnSRC_SYSTEM_OFF)
    }
}

BUTTON_EVENT[dvMainKey,conf]{
    PUSH:{
	DO_PUSH(dvTP,btnSRC_VID_CONF)
    }
    HOLD[30]:{
	DO_PUSH(dvTP,btnSRC_SYSTEM_OFF)
    }
}

BUTTON_EVENT[dvMainKey,dvd]{
    PUSH:{
	DO_PUSH(dvTP,btnSRC_DVD_VCR)
    }
    HOLD[30]:{
	DO_PUSH(dvTP,btnSRC_SYSTEM_OFF)
    }
}

BUTTON_EVENT[dvMainKey,table]{
    PUSH:{
	switch(tp_last_source_selected){
	    CASE btnSRC_TABLE_1_RGB:{
		DO_PUSH(dvTP,btnSRC_TABLE_2_RGB)
	    }
	    CASE btnSRC_TABLE_2_RGB:{
		DO_PUSH(dvTP,btnSRC_TABLE_3_RGB)
	    }
	    CASE btnSRC_TABLE_3_RGB:{
		DO_PUSH(dvTP,btnSRC_TABLE_4_RGB)
	    }
	    CASE btnSRC_TABLE_4_RGB:{
		DO_PUSH(dvTP,btnSRC_TABLE_1_RGB)
	    }
	    DEFAULT:{
		DO_PUSH(dvTP,btnSRC_TABLE_1_RGB)
	    }
	}
    }
    HOLD[30]:{
	DO_PUSH(dvTP,btnSRC_SYSTEM_OFF)
    }
}

BUTTON_EVENT[dvMainKey,sat]{
    PUSH:{
	DO_PUSH(dvTP,btnSRC_SAT)
    }
    HOLD[30]:{
	DO_PUSH(dvTP,btnSRC_SYSTEM_OFF)
    }
}
BUTTON_EVENT[dvMainKey,navup]{
    PUSH:{
	DO_PUSH(dvTP,btnEXT_CUR_UP)
    }
}
BUTTON_EVENT[dvMainKey,navdn]{
    PUSH:{
	DO_PUSH(dvTP,btnEXT_CUR_DOWN)
    }
}
BUTTON_EVENT[dvMainKey,navleft]{
    PUSH:{
	DO_PUSH(dvTP,btnEXT_CUR_LEFT)
    }
}
BUTTON_EVENT[dvMainKey,navright]{
    PUSH:{
	DO_PUSH(dvTP,btnEXT_CUR_RIGHT)
    }
}
BUTTON_EVENT[dvMainKey,navselect]{
    PUSH:{
	DO_PUSH(dvTP,btnEXT_ENTER)
    }
}
BUTTON_EVENT[dvMainKey,volup]{
    PUSH:{
	DO_PUSH(dvTP,btnPROG_VOL_UP)
    }
}
BUTTON_EVENT[dvMainKey,voldn]{
    PUSH:{
	DO_PUSH(dvTP,btnPROG_VOL_DOWN)
    }
}
BUTTON_EVENT[dvLightsKey,lightson]{
    PUSH:{
	DO_PUSH(dvTP,btnLIGHTS_PRESET_1) 
    }
}
BUTTON_EVENT[dvLightsKey,lightspresent]{
    PUSH:{
	DO_PUSH(dvTP,btnLIGHTS_PRESET_2) 
    }
}
BUTTON_EVENT[dvLightsKey,lightsdvd]{
    PUSH:{
	DO_PUSH(dvTP,btnLIGHTS_PRESET_3)
    }
}
BUTTON_EVENT[dvLightsKey,lightswork]{
    PUSH:{
	DO_PUSH(dvTP,btnLIGHTS_PRESET_4)
    }
}
BUTTON_EVENT[dvLightsKey,lightsoff]{
    PUSH:{
	DO_PUSH(dvTP,btnLIGHTS_OFF)
    }
}
BUTTON_EVENT[dvLightsKey,shadesopen]{
    PUSH:{
	DO_PUSH(dvTP,btnSHADES_OPEN) 
    }
}
BUTTON_EVENT[dvLightsKey,shadesclose]{
    PUSH:{
	DO_PUSH(dvTP,btnSHADES_CLOSE) 
    }
}


DEFINE_PROGRAM

[dvMainKey,lecturn] = (current_src[DEST_ROOM] == SRC_PODIUM_1_PC || current_src[DEST_ROOM] == SRC_PODIUM_1_LAPTOP_DVI|| current_src[DEST_ROOM] == SRC_PODIUM_1_LAPTOP_RGB)
[dvMainKey,blu_ray] = (current_src[DEST_ROOM] == SRC_HDDVD)
[dvMainKey,conf] = (current_src[DEST_ROOM] == SRC_VID_CONF)
[dvMainKey,dvd]	 = (current_src[DEST_ROOM] == SRC_DVD_VCR)
[dvMainKey,table] = (current_src[DEST_ROOM] == SRC_TABLE_1_RGB || current_src[DEST_ROOM] == SRC_TABLE_2_RGB || current_src[DEST_ROOM] == SRC_TABLE_3_RGB || current_src[DEST_ROOM] == SRC_TABLE_4_RGB)
[dvMainKey,sat] = (current_src[DEST_ROOM] == SRC_SAT)


[dvLightsKey,lightsoff]      = ( lutron_scene_fb[1] == 0 );
[dvLightsKey,lightson] = ( lutron_scene_fb[1] == 1 );
[dvLightsKey,lightspresent] = ( lutron_scene_fb[1] == 2 );
[dvLightsKey,lightsdvd] = ( lutron_scene_fb[1] == 3 );
[dvLightsKey,lightswork] = ( lutron_scene_fb[1] == 4 );
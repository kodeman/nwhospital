PROGRAM_NAME='shades'
//=========================================================//
//=========================================================//
DEFINE_CONSTANT
//---------------------------------------------------------//


//=========================================================//
//=========================================================//
DEFINE_TYPE
//---------------------------------------------------------//


//=========================================================//
//=========================================================//
DEFINE_VARIABLE
//---------------------------------------------------------//


//=========================================================//
//=========================================================//
define_function do_shades(char cmd[])
{
 switch(cmd)
 {
	case cmdSHADES_OPEN:   
	{
	 off[dvRELAYS,rlySHADES_CLOSE]
	 off[dvRELAYS,rlySHADES_MIDDLE]
	 on[dvRELAYS,rlySHADES_OPEN]
   wait 1 off[dvRELAYS,rlySHADES_OPEN]
  }
	case cmdSHADES_CLOSE:
	{
	 off[dvRELAYS,rlySHADES_OPEN]
	 off[dvRELAYS,rlySHADES_MIDDLE]
	 on[dvRELAYS,rlySHADES_CLOSE]
   wait 1 off[dvRELAYS,rlySHADES_CLOSE]
	}
	case cmdSHADES_MIDDLE:
	{
	 off[dvRELAYS,rlySHADES_OPEN]
	 off[dvRELAYS,rlySHADES_CLOSE]
	 on[dvRELAYS,rlySHADES_MIDDLE]
   wait 1 off[dvRELAYS,rlySHADES_MIDDLE]
	}
	default: {  }
 }
}
//-----------------------------------------------//
define_function do_shades_for_src(integer src)
{
 switch(src)
 {
  case SRC_VID_CONF           : { do_shades(cmdSHADES_CLOSE); }
  case SRC_PODIUM_1_PC        : {  }
		case SRC_PODIUM_1_LAPTOP_DVI: {  }
		case SRC_PODIUM_1_LAPTOP_RGB: {  }
		case SRC_PODIUM_2_PC        : {  }
		case SRC_PODIUM_2_LAPTOP_DVI: {  }
		case SRC_PODIUM_2_LAPTOP_RGB: {  }
  case SRC_TABLE_1_RGB        : {  }
  case SRC_TABLE_2_RGB        : {  }
  case SRC_RACK_VGA           : {  }
  case SRC_RACK_SVIDEO        : {  }
  case SRC_DVD_VCR            : { do_shades(cmdSHADES_CLOSE); }
		case SRC_HDDVD              : { do_shades(cmdSHADES_CLOSE); }
		case SRC_SAT                : { do_shades(cmdSHADES_CLOSE); }
  case SRC_SYSTEM_OFF         : { do_shades(cmdSHADES_OPEN); }
	default: {  }
 }
}
//=========================================================//
//=========================================================//
DEFINE_START
//---------------------------------------------------------//


//=========================================================//
//=========================================================//
DEFINE_EVENT
//---------------------------------------------------------//


//=========================================================//
//=========================================================//
DEFINE_PROGRAM
//---------------------------------------------------------//


//=========================================================//
//                     END OF PROGRAM                      //
//        DO NOT PUT ANY CODE BELOW THIS COMMENT           //
//=========================================================//
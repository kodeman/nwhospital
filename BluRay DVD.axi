PROGRAM_NAME='BluRay DVD'
//=========================================================//
//=========================================================//
DEFINE_CONSTANT
//---------------------------------------------------------//


//=========================================================//
//=========================================================//
DEFINE_TYPE
//---------------------------------------------------------//


//=========================================================//
//=========================================================//
DEFINE_VARIABLE
//---------------------------------------------------------//


//=========================================================//
//=========================================================//
define_function do_hddvd_cmd(char cmd[])
{
 switch(cmd)
 {
	 case cmdPOWER_ON:   { send_command dvHDDVD,"'sp'"; }
	 case cmdPOWER_OFF:  { send_command dvHDDVD,"'sp'"; }
	 case cmdPLAY:       { send_command dvHDDVD,"'sp',1"; }
	 case cmdSTOP:       { send_command dvHDDVD,"'sp',2"; }
	 case cmdPAUSE:      { send_command dvHDDVD,"'sp',3"; }
	 case cmdTRACK_FWD:  { send_command dvHDDVD,"'sp',4"; }
	 case cmdTRACK_BACK: { send_command dvHDDVD,"'sp',5"; }
	 case cmdFFWD:       { send_command dvHDDVD,"'sp',6"; }
	 case cmdREW:        { send_command dvHDDVD,"'sp',7"; }
	 case cmdTOP_MENU:   { send_command dvHDDVD,"'sp',65"; }
  //case cmdSETUP:      { send_command dvHDDVD,"'sp'"; }
  case cmdMENU:       { send_command dvHDDVD,"'sp',44"; }
  case cmdCUR_UP:     { send_command dvHDDVD,"'sp',45"; }
  case cmdCUR_DOWN:   { send_command dvHDDVD,"'sp',46"; }
  case cmdCUR_LEFT:   { send_command dvHDDVD,"'sp',47"; }
  case cmdCUR_RIGHT:  { send_command dvHDDVD,"'sp',48"; }
  case cmdSELECT:     { send_command dvHDDVD,"'sp',49"; }
	 case cmdSUBTITLE:   { send_command dvHDDVD,"'sp',57"; }
	 case cmdRETURN:     { send_command dvHDDVD,"'sp',54"; }
	 case cmdRED:        { send_command dvHDDVD,"'sp',112"; }
	 case cmdGREEN:      { send_command dvHDDVD,"'sp',113"; }
	 case cmdBLUE:       { send_command dvHDDVD,"'sp',114"; }
	 case cmdYELLOW:     { send_command dvHDDVD,"'sp',115"; }
	 default: {  }
 }
}
//=========================================================//
//=========================================================//
DEFINE_START
//---------------------------------------------------------//


//=========================================================//
//=========================================================//
DEFINE_EVENT
//---------------------------------------------------------//
data_event[dvHDDVD]
{
 online:
 {
  send_command data.device,"'CTON',2";
	 send_command data.device,"'CTOF',4";
	 send_command data.device,"'SET MODE IR'";
	 send_command data.device,"'CARON'";
 }
}

//=========================================================//
//=========================================================//
DEFINE_PROGRAM
//---------------------------------------------------------//


//=========================================================//
//                     END OF PROGRAM                      //
//        DO NOT PUT ANY CODE BELOW THIS COMMENT           //
//=========================================================//
MODULE_NAME='Panasonic TH-103PF10UK'(DEV vdvDATA, DEV dvTHEPORT)
//==========================================================================//
//==========================================================================//
DEFINE_CONSTANT
//--------------------------------------------------------------------------//
//----------------------------------------------------------------//
MAX_BUSY_TIME = 10;
MAX_QUE_SIZE  = 100;    (*MAX SIZE OF QUE*)

TRUE  =1;
FALSE =0;

WARM_TIME = 70;
COOL_TIME = 20;

STAT_POWER_OFF = 1;
STAT_POWER_ON  = 2;
CHAN_POWER_ON  = 1;
CHAN_POWER_OFF = 2;

POWER_ON      = 1;
POWER_OFF     = 2;
NUM_POWER     = 2;

INPUT_PC      = 1;
INPUT_SLOT_1  = 2;
INPUT_SLOT_2  = 3;
INPUT_SLOT_3  = 4;
INPUT_SLOT_1A = 5;
INPUT_SLOT_1B = 6;
INPUT_SLOT_2A = 7;
INPUT_SLOT_2B = 8;
NUM_INPUT     = 8;
//==========================================================================//
DEFINE_TYPE
//--------------------------------------------------------------------------//
structure _sQUE_TYPE
{
  integer   head;
  integer   tail;
  char      que[MAX_QUE_SIZE][20];
}

//==========================================================================//
DEFINE_VARIABLE
//--------------------------------------------------------------------------//
_sQUE_TYPE theque;
integer counter = 0;
char device_idle = false;
char the_cmd;
char cmd_data[255];
char junk;
char temp[255];
char output[255];
char rtn_msg[255];
char last_sent;
char buffer[100];
char status_power = STAT_POWER_OFF;
char new_input    = 0;

//-- TV COMMANDS... --//
char strPOWER_CMDS[NUM_POWER][20];
char strINPUT_CMDS[NUM_INPUT][20];
//==========================================================================//
//--------------------------------------------------------------------------//
define_function init_device()
{
 theque.head = 1;
	theque.tail = 1;
 device_idle=FALSE;
 wait 20 device_idle=TRUE;
	Send_Command dvTHEPORT,'SET BAUD 9600,N,8,1,485 DISABLE';
	send_command dvTHEPORT,'HSOFF';
}

//-----------------------------------------------------------//
define_function handle_timeout()
{
 device_idle=TRUE;
	send_string vdvDATA, "'ERROR: ', __FILE__,' COMMAND TIMED OUT'";
}

//-----------------------------------------------------------//
define_function deque()
{
 local_var char themsg[255];

 if(device_idle==TRUE)           //SOMETHING TO DEQUE
 {
  if(theque.head!=theque.tail)
  {
   themsg = theque.que[theque.head];
   theque.head++;

   if(theque.head > MAX_QUE_SIZE)
    theque.head = 1;
   send_string dvTHEPORT, "themsg";
   //SEND_STRING 0,"'SENDING STRING TO DEVICE: ',THEMSG,' !!!!'"
   device_idle=FALSE;
   wait MAX_BUSY_TIME 'DEVICE TIMEOUT' { handle_timeout(); }
  }
  else            //NOTHING TO DEQUE SO SEND STATUS REQUEST
  {
//  SEND_STRING dvTHEPORT, STATUS_REQUEST
//  DEVICE_IDLE=FALSE
  }
 }
}

//-----------------------------------------------------------//
define_function enque(char THEMSG[255])
{
  local_var integer next_index;
  
		//send_string vdvDATA,"'enque() called!'";
		
  next_index = theque.tail + 1;
  if(next_index > MAX_QUE_SIZE)
   next_index = 1;
			
  if(next_index == theque.head)
  {
   // QUE IS FULL - EVERYBODY PANIC - SEND MESSAGE TO MAINLINE
   send_string vdvDATA, "'ERROR: ', __FILE__,' QUE IS FULL!'";
  }    
  else
		{
		 //send_string vdvDATA,"'theque.tail =',itoa(theque.tail)";
   theque.que[theque.tail] = THEMSG;
   theque.tail = next_index;
			//send_string vdvDATA,"'enqueing!'";
		}
}

//-----------------------------------------------------------//
define_function parse_buffer()
{
 IF(length_string(buffer))
 {
	 //send_string 0,"__file__,' parse_buffer(): length_string(buffer)!'";
	 IF(find_string(buffer, "$03", 1))
	 {
			cancel_wait 'DEVICE TIMEOUT';
			rtn_msg = remove_string(buffer, "$03", 1);
	  device_idle=TRUE;
   
	  select
	  {
				active(find_string(rtn_msg,"'OK'",1)):  (*COMMAND ACCEPTED AND EXECUTED*)
    {
     device_idle=TRUE;
	    //send_string 0,"__FILE__,' DATA_EVENT[dvTHEPORT]: FOUND "OK"!'";
    }
    active(find_string(rtn_msg,"'ER'",1)):  (*COMMAND NOT ACCEPTED AND EXECUTED*)
    {
     device_idle=TRUE;
     //send_string vdvDATA, "'ERROR: ',__FILE__,' GOT ERROR RESPONSE FROM DEVICE'";
	   }
	  }
	 }
 }
}
//-----------------------------------------------------------//
define_function do_power_cmd(char power[])
{
 switch(power)
	{
	 case '0':
		{
			enque("strPOWER_CMDS[POWER_OFF]");
			wait COOL_TIME 'cool wait' { status_power = STAT_POWER_OFF; }
		}
		case '1':
		{
		 if(status_power == STAT_POWER_OFF)
			{
			 enque("strPOWER_CMDS[POWER_ON]");
			 wait WARM_TIME 'warm wait' { status_power = STAT_POWER_ON; } 
			}
		}
		default: {  }
	}
}
//-----------------------------------------------------------//
define_function do_input_cmd(char input)
{
 send_string 0,"__file__,' do_input_cmd() called'";
 if(length_array(strINPUT_CMDS[input]))
	{
  enque(strINPUT_CMDS[input]); 
		send_string 0,"__file__,' enqueing input command'";
	}
}
//==========================================================================//
define_event
//--------------------------------------------------------------------------//
// PROCESS COMMAND REQUESTS FROM MAIN PROGRAM
data_event[vdvDATA]
{
 string:
	{
	}
	command:
 {
		stack_var char cCMD[100];
  
  if (!find_string(DATA.TEXT, 'PASSTHRU=', 1)) 
	 { 
	  DATA.TEXT = upper_string(DATA.TEXT);
	 }
		if(find_string(DATA.TEXT, '=', 1))
	 { 
	  cCMD = remove_string(DATA.TEXT, '=', 1);
	 }
  else
	 {
	  if (find_string(DATA.TEXT, '?', 1)) 
	  { 
	   cCMD = remove_string(DATA.TEXT, '?', 1);
	  }
	  else 
	  { 
	   cCMD = DATA.TEXT;
	  } 
		}	
		switch(cCMD)
  {
   // FIND MATCHING COMMAND AND PARSE REST OF MESSAGE. CONVERT MSG TO A CMD TO BE
   // SENT TO THE PHYSICAL DEVICE
			case 'POWER=':
			{
    switch(data.text)
    {
     case '0':
     {
						do_power_cmd("data.text");
     }
     case '1':
     {
						do_power_cmd("data.text");
					}
					case 'T':
					{
					 if(status_power == 1) // if on...
						{
						 do_power_cmd("'0'"); // turn it off
						}
						else // else it's off
						{
						 do_power_cmd("'1'"); // turn it on
						}
					}
     default : 
	    { 
	     //SEND_STRING vdvDEV, "'ERRORM=Invalid value for Power cmd.'" 
	    }
				}
			}
   case 'INPUT=':
   {
			 send_string 0,"__file__,' INPUT='";
	   switch(data.text)
    {
     case 'SLOT_1A': { new_input = INPUT_SLOT_1A;	send_string 0,"__file__,' SLOT_1A'"; }
					case 'SLOT_1B': { new_input = INPUT_SLOT_1B;	send_string 0,"__file__,' SLOT_1B'"; }
					case 'SLOT_1' : { new_input = INPUT_SLOT_1;	 send_string 0,"__file__,' SLOT_1'"; }
					case 'SLOT_2A': { new_input = INPUT_SLOT_2A;	send_string 0,"__file__,' SLOT_2A'"; }
					case 'SLOT_2B': { new_input = INPUT_SLOT_2B;	send_string 0,"__file__,' SLOT_2B'"; }
					case 'SLOT_2' : { new_input = INPUT_SLOT_2;	 send_string 0,"__file__,' SLOT_2'"; }
					case 'SLOT_3' : { new_input = INPUT_SLOT_3;	 send_string 0,"__file__,' SLOT_3'"; }
					case 'PC'     : { new_input = INPUT_PC;	     send_string 0,"__file__,' PC'"; }
     default:  {  }
				}
			}
			case 'VOLUME=':
   {
    switch(DATA.TEXT)
    {
     case '+': 
     case '-': 
     default : 
					{
					}
    }
	  }
			case 'MUTE=':
			{
			 switch(DATA.TEXT)
    {
			  case '0':  {  }
     case '1':  {  }
					case 'T':  {  }
     default :  {  }
				}
			}
			case 'CHANNEL=':
   {
    switch(DATA.TEXT)
    {
     case '+': {  }
     case '-': {  }
     default : 
					{
					}
    }
	  }
			case 'ASPECT=':
			{
			 switch(DATA.TEXT)
    {
     case '4x3':  
					{
					}
     case 'FULL': 
					{
					}
					case 'ZOOM': 
					{
					}
     default : 
					{
					}
    }
			}
		 case 'PASSTHRU=': 
	  { 
	  }
			case 'POWER?':
			{  
			}   
			case 'VERSION?':
	  { 
   }
   default: 
  	{ 
	   //SEND_STRING vdvDEV, "'ERRORM=Unknown Command.'" 
	  }
		}
	}
}
//-----------------------------------------------------------//
// process stuff coming back from the projector
data_event[dvTHEPORT]
{
 online:
 {
  init_device();
 }
 
 offline:
 {
  device_idle = FALSE;
 }
 
 string:
 {
  buffer = "buffer,data.text";
 }
}

//==========================================================================//    
define_start
//--------------------------------------------------------------------------//
//-- TV COMMANDS... --//
// power
strPOWER_CMDS[POWER_ON]  = "$02,'PON',$03";
strPOWER_CMDS[POWER_OFF] = "$02,'POF',$03";
// input
strINPUT_CMDS[INPUT_PC]      = "$02,'IMS:PC1',$03";
strINPUT_CMDS[INPUT_SLOT_1]  = "$02,'IMS:SL1',$03";
strINPUT_CMDS[INPUT_SLOT_2]  = "$02,'IMS:SL2',$03";
strINPUT_CMDS[INPUT_SLOT_3]  = "$02,'IMS:SL3',$03";
strINPUT_CMDS[INPUT_SLOT_1A] = "$02,'IMS:SL1A',$03";
strINPUT_CMDS[INPUT_SLOT_1B] = "$02,'IMS:SL1B',$03";
strINPUT_CMDS[INPUT_SLOT_2A] = "$02,'IMS:SL2A',$03";
strINPUT_CMDS[INPUT_SLOT_2B] = "$02,'IMS:SL2B',$03";

//==========================================================================//
DEFINE_PROGRAM
//--------------------------------------------------------------------------//
deque();
parse_buffer();
// select new input if projector is powered on...
if(status_power == STAT_POWER_ON && new_input <> 0)
{
 send_string 0,"__file__,' calling do_input_cmd()'";
 do_input_cmd(new_input);
	new_input = 0;
}

// power feedback...
[vdvDATA,CHAN_POWER_ON]  = (status_power == STAT_POWER_ON);
[vdvDATA,CHAN_POWER_OFF] = (status_power == STAT_POWER_OFF);
PROGRAM_NAME='sat'
//=========================================================//
//=========================================================//
DEFINE_CONSTANT
//---------------------------------------------------------//


//=========================================================//
//=========================================================//
DEFINE_TYPE
//---------------------------------------------------------//


//=========================================================//
//=========================================================//
DEFINE_VARIABLE
//---------------------------------------------------------//


//=========================================================//
//=========================================================//
define_function do_sat_cmd(char cmd[])
{
 switch(cmd)
 {
	 case cmdPLAY:       { send_command dvSAT,"'sp',85"; }
	 case cmdSTOP:       { send_command dvSAT,"'sp',86"; }
	 case cmdPAUSE:      { send_command dvSAT,"'sp',87"; }
	 case cmdTRACK_FWD:  { send_command dvSAT,"'sp',90"; }
	 case cmdTRACK_BACK: { send_command dvSAT,"'sp',91"; }
	 case cmdFFWD:       { send_command dvSAT,"'sp',88"; }
	 case cmdREW:        { send_command dvSAT,"'sp',89"; }
		case cmdRECORD:     { send_command dvSAT,"'sp',92"; }
  case cmdMENU:       { send_command dvSAT,"'sp',44"; }
  case cmdCUR_UP:     { send_command dvSAT,"'sp',45"; }
  case cmdCUR_DOWN:   { send_command dvSAT,"'sp',46"; }
  case cmdCUR_LEFT:   { send_command dvSAT,"'sp',47"; }
  case cmdCUR_RIGHT:  { send_command dvSAT,"'sp',48"; }
  case cmdSELECT:     { send_command dvSAT,"'sp',49"; }
  case cmdPAGE_UP:    { send_command dvSAT,"'sp',72"; }
  case cmdPAGE_DOWN:  { send_command dvSAT,"'sp',73"; }
  case cmdGUIDE:      { send_command dvSAT,"'sp',53"; }
  case cmdRECALL:     { send_command dvSAT,"'sp',55"; }
  case cmdCANCEL:     { send_command dvSAT,"'sp',43"; }
  case cmdLIVE_TV:    { send_command dvSAT,"'sp',60"; }
  case cmdDVR:        { send_command dvSAT,"'sp',81"; }
  case cmdNUM_1:      { send_command dvSAT,"'sp',11"; }
  case cmdNUM_2:      { send_command dvSAT,"'sp',12"; }
  case cmdNUM_3:      { send_command dvSAT,"'sp',13"; }
  case cmdNUM_4:      { send_command dvSAT,"'sp',14"; }
  case cmdNUM_5:      { send_command dvSAT,"'sp',15"; }
  case cmdNUM_6:      { send_command dvSAT,"'sp',16"; }
  case cmdNUM_7:      { send_command dvSAT,"'sp',17"; }
  case cmdNUM_8:      { send_command dvSAT,"'sp',18"; }
  case cmdNUM_9:      { send_command dvSAT,"'sp',19"; }
  case cmdNUM_0:      { send_command dvSAT,"'sp',10"; }
  case cmdSTAR:       { send_command dvSAT,"'sp',78"; }
  case cmdPOUND:      { send_command dvSAT,"'sp',80"; }
	 default: {  }
 }
}
//=========================================================//
//=========================================================//
DEFINE_START
//---------------------------------------------------------//


//=========================================================//
//=========================================================//
DEFINE_EVENT
//---------------------------------------------------------//
data_event[dvSAT]
{
 online:
 {
  send_command data.device,"'CTON',2";
	 send_command data.device,"'CTOF',4";
	 send_command data.device,"'SET MODE IR'";
	 send_command data.device,"'CARON'";
 }
}

//=========================================================//
//=========================================================//
DEFINE_PROGRAM
//---------------------------------------------------------//


//=========================================================//
//                     END OF PROGRAM                      //
//        DO NOT PUT ANY CODE BELOW THIS COMMENT           //
//=========================================================//
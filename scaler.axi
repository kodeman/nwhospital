PROGRAM_NAME='scaler'
//=========================================================//
//=========================================================//
DEFINE_CONSTANT
//---------------------------------------------------------//

//=========================================================//
//=========================================================//
DEFINE_TYPE
//---------------------------------------------------------//


//=========================================================//
//=========================================================//
DEFINE_VARIABLE
//---------------------------------------------------------//


//=========================================================//
//=========================================================//
define_function do_scaler_input(integer src)
{
 switch(src)
	{
  case SRC_VID_CONF           : { send_string dvSCALER,"'4!4*6\'"; } // select input 4, force input 4 to rgb scaled
  case SRC_PODIUM_1_PC        : { send_string dvSCALER,"''"; }
  case SRC_PODIUM_1_LAPTOP_DVI: { send_string dvSCALER,"''"; }
  case SRC_PODIUM_1_LAPTOP_RGB: { send_string dvSCALER,"'4!4*6\'"; } // select input 4, force input 4 to rgb scaled
  case SRC_PODIUM_2_PC        : { send_string dvSCALER,"''"; } 
  case SRC_PODIUM_2_LAPTOP_DVI: { send_string dvSCALER,"''"; } 
  case SRC_PODIUM_2_LAPTOP_RGB: { send_string dvSCALER,"'4!4*6\'"; } // select input 4, force input 4 to rgb scaled
  case SRC_TABLE_1_RGB        : { send_string dvSCALER,"'4!4*6\'"; } // select input 4, force input 4 to rgb scaled
  case SRC_TABLE_2_RGB        : { send_string dvSCALER,"'4!4*6\'"; } // select input 4, force input 4 to rgb scaled
  case SRC_TABLE_3_RGB        : { send_string dvSCALER,"'4!4*6\'"; } // select input 4, force input 4 to rgb scaled
  case SRC_TABLE_4_RGB        : { send_string dvSCALER,"'4!4*6\'"; } // select input 4, force input 4 to rgb scaled
  case SRC_RACK_VGA           : { send_string dvSCALER,"'4!4*6\'"; } // select input 4, force input 4 to rgb scaled
  case SRC_RACK_SVIDEO        : { send_string dvSCALER,"'2!2*2\'"; } // select input 2, force input 2 to svideo
  case SRC_DVD_VCR            : { send_string dvSCALER,"'2!2*2\'"; } // select input 2, force input 2 to svideo
  case SRC_HDDVD              : { send_string dvSCALER,"''"; }
  case SRC_SAT                : { send_string dvSCALER,"''"; }
  case SRC_CAM_1              : { send_string dvSCALER,"''"; }
  case SRC_CAM_2              : { send_string dvSCALER,"''"; }
  case SRC_SYSTEM_OFF         : {  }
		default: {  }
	}
}
//-----------------------------------------------//
//=========================================================//
//=========================================================//
DEFINE_START
//---------------------------------------------------------//


//=========================================================//
//=========================================================//
DEFINE_EVENT
//---------------------------------------------------------//
data_event[dvSCALER]
{
 online:
	{
	 send_command data.device,"'SET BAUD 9600,N,8,1,485 DISABLE'";
		//send_command data.device,"'19*06='"; // for output resolution to 1080p / 120Hz
	}
	string:
	{
	
	}
}

//=========================================================//
//=========================================================//
DEFINE_PROGRAM
//---------------------------------------------------------//


//=========================================================//
//                     END OF PROGRAM                      //
//        DO NOT PUT ANY CODE BELOW THIS COMMENT           //
//=========================================================//
PROGRAM_NAME='codec'
//=========================================================//
//=========================================================//
DEFINE_CONSTANT
//---------------------------------------------------------//
CODEC_CMD_CALL      = 'control remote call';
CODEC_CMD_TRIANGLE  = 'control remote tri';
CODEC_CMD_SQUARE    = 'control remote squ';
CODEC_CMD_CIRCLE    = 'control remote cir';
CODEC_CMD_BACK      = 'control remote back';
CODEC_CMD_OK        = 'control remote ok';
CODEC_CMD_CUR_UP    = 'control remote up';
CODEC_CMD_CUR_DOWN  = 'control remote down';
CODEC_CMD_CUR_LEFT  = 'control remote left';
CODEC_CMD_CUR_RIGHT = 'control remote right';
CODEC_CMD_VOL_UP    = 'control remote vup';
CODEC_CMD_VOL_DOWN  = 'control remote vdn';
CODEC_CMD_MUTE      = 'control remote mute';
CODEC_CMD_ZOOM_IN   = 'control remote zin';
CODEC_CMD_ZOOM_OUT  = 'control remote zout';
CODEC_CMD_NEAR      = 'control remote near';
CODEC_CMD_FAR       = 'control remote far';
CODEC_CMD_0         = 'control remote 0';
CODEC_CMD_1         = 'control remote 1';
CODEC_CMD_2         = 'control remote 2';
CODEC_CMD_3         = 'control remote 3';
CODEC_CMD_4         = 'control remote 4';
CODEC_CMD_5         = 'control remote 5';
CODEC_CMD_6         = 'control remote 6';
CODEC_CMD_7         = 'control remote 7';
CODEC_CMD_8         = 'control remote 8';
CODEC_CMD_9         = 'control remote 9';
CODEC_CMD_POUND     = 'control remote #';
CODEC_CMD_STAR      = 'control remote *';

//=========================================================//
//=========================================================//
DEFINE_TYPE
//---------------------------------------------------------//


//=========================================================//
//=========================================================//
DEFINE_VARIABLE
//---------------------------------------------------------//
char codec_buf[1000];
integer in_call_fb = 0; // 1 if in call, 0 if not in call

//=========================================================//
//=========================================================//
define_function do_codec_cmd(char cmd[])
{
 send_string dvCODEC,"cmd,$0D,$0A"
}
//-----------------------------------------------//
define_function do_codec_cam_ctrl(char cmd[])
{
	select
	 {
			active(find_string(cmd, cmdSTOP, 1)):       { send_string dvCODEC,"'set camera position -s',$0D,$0A" }
			active(find_string(cmd, cmdCUR_UP, 1)):     { send_string dvCODEC,"'set camera position -u',$0D,$0A" }
			active(find_string(cmd, cmdCUR_DOWN, 1)):   { send_string dvCODEC,"'set camera position -d',$0D,$0A" }
			active(find_string(cmd, cmdCUR_LEFT, 1)):   { send_string dvCODEC,"'set camera position -l',$0D,$0A" }
			active(find_string(cmd, cmdCUR_RIGHT, 1)):  { send_string dvCODEC,"'set camera position -r',$0D,$0A" }
			active(find_string(cmd, cmdZOOM_IN, 1)):    { send_string dvCODEC,"'set camera position -n',$0D,$0A" }
   active(find_string(cmd, cmdZOOM_OUT, 1)):   { send_string dvCODEC,"'set camera position -f',$0D,$0A" }
	  active(1): {  }
	 }
}
//-----------------------------------------------//
define_function do_src_for_codec(integer src)
{
 switch(src)
	{
	 //case SRC_PODIUM_RGB:  { send_string dvCODEC,"'set video primary-input vga0',$0D,$0A"; } // vga input
		case SRC_PODIUM_1_PC: { send_string dvCODEC,"'set video primary-input vga0',$0D,$0A"; } // vga input
		case SRC_PODIUM_2_PC: { send_string dvCODEC,"'set video primary-input vga0',$0D,$0A"; } // vga input
		case SRC_PODIUM_1_LAPTOP_RGB: { send_string dvCODEC,"'set video primary-input vga0',$0D,$0A"; } // vga input
		case SRC_PODIUM_2_LAPTOP_RGB: { send_string dvCODEC,"'set video primary-input vga0',$0D,$0A"; } // vga input
	 case SRC_TABLE_1_RGB: { send_string dvCODEC,"'set video primary-input vga0',$0D,$0A"; } // vga input
	 case SRC_TABLE_2_RGB: { send_string dvCODEC,"'set video primary-input vga0',$0D,$0A"; } // vga input?
		case SRC_TABLE_3_RGB: { send_string dvCODEC,"'set video primary-input vga0',$0D,$0A"; } // vga input
	 case SRC_TABLE_4_RGB: { send_string dvCODEC,"'set video primary-input vga0',$0D,$0A"; } // vga input?
	 case SRC_RACK_VGA:    { send_string dvCODEC,"'set video primary-input vga0',$0D,$0A"; } // vga input?
	 case SRC_RACK_SVIDEO: { send_string dvCODEC,"'set video primary-input sd0',$0D,$0A"; } // vga input?
	 case SRC_DVD_VCR:     { send_string dvCODEC,"'set video primary-input sd0',$0D,$0A"; } // vga input?
	 case SRC_CAM_1:       { send_string dvCODEC,"'set video primary-input hd0',$0D,$0A";  } // hd 1 input
	 case SRC_CAM_2:       { send_string dvCODEC,"'set video primary-input hd1',$0D,$0A";  } // hd 2 input
	 default: {  }
	}
}
//-----------------------------------------------//
define_function codec_start_presentation()
{
 wait 10 send_string dvCODEC,"'control call presentation 1 start',$0D,$0A";
}
//-----------------------------------------------//
define_function codec_stop_presentation()
{
	wait 10 send_string dvCODEC,"'control call presentation 1 stop',$0D,$0A";
}
//-----------------------------------------------//
define_function codec_hangup_call()
{
 send_string dvCODEC,"'control call hangup -a',$0D,$0A";
}
//=========================================================//
//=========================================================//
DEFINE_START
//---------------------------------------------------------//


//=========================================================//
//=========================================================//
DEFINE_EVENT
//---------------------------------------------------------//
data_event[dvCODEC] // RS232 device
{
 online:
	{
		// set baud, etc...
		send_command data.device,"'SET BAUD 9600,N,8,1,485 DISABLE'";
		send_command data.device,"'HSOFF'"; 
	}
	string:
	{
	 local_var char junk[255];
	 codec_buf = data.text;
		if(find_string(codec_buf, "'Answered'", 1)) 
		{
		 junk = remove_string(codec_buf, "'Answered'", 1);
		 in_call_fb = 	1;
			send_string 0,"'codec in call'";
		}
		if(find_string(codec_buf, "'Connected'", 1) ) 
		{
		 junk = remove_string(codec_buf, "'Connected'", 1);
		 in_call_fb = 	1;
			send_string 0,"'codec in call'";
		}
		if(find_string(codec_buf, "'Terminated'", 1) ) 
		{
		 junk = remove_string(codec_buf, "'Terminated'", 1);
		 in_call_fb = 	0;
			send_string 0,"'codec not in call'";
		}
		if(find_string(codec_buf, "'hangup'", 1) ) 
		{
		 junk = remove_string(codec_buf, "'hangup'", 1);
		 in_call_fb = 	0;
			send_string 0,"'codec not in call'";
		}
	}
}

//=========================================================//
//=========================================================//
DEFINE_PROGRAM
//---------------------------------------------------------//


//=========================================================//
//                     END OF PROGRAM                      //
//        DO NOT PUT ANY CODE BELOW THIS COMMENT           //
//=========================================================//
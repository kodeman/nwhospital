PROGRAM_NAME='northwest hospital, main v1'
//=========================================================//
//=========================================================//
//  FILE_LAST_MODIFIED_ON: 04/05/2006  AT: 09:00:25        //
//=========================================================//
// System Type : NetLinx                                   //
//=========================================================//
// REV HISTORY:                                            //
//=========================================================//
(*
    $History: $
*)

//=========================================================//
//=========================================================//
DEFINE_CONSTANT
//---------------------------------------------------------//
// podium locations...
POD_LOCATION_1 = 1;
POD_LOCATION_2 = 2;
NUM_PODIUM_LOC = 2;

// sources...
SRC_VID_CONF             = 1;
SRC_PODIUM_1_PC          = 2;
SRC_PODIUM_1_LAPTOP_DVI  = 3;
SRC_PODIUM_1_LAPTOP_RGB  = 4;
SRC_PODIUM_2_PC          = 5;
SRC_PODIUM_2_LAPTOP_DVI  = 6;
SRC_PODIUM_2_LAPTOP_RGB  = 7;
SRC_TABLE_1_RGB          = 8;
SRC_TABLE_2_RGB          = 9;
SRC_TABLE_3_RGB          = 10;
SRC_TABLE_4_RGB          = 11;
SRC_RACK_VGA             = 12;
SRC_RACK_SVIDEO          = 13;
SRC_DVD_VCR              = 14;
SRC_HDDVD                = 15;
SRC_SAT                  = 16;
SRC_SYSTEM_OFF           = 17;
SRC_CAM_1                = 18;
SRC_CAM_2                = 19;
NUM_SRC                  = 19;
// destinations...
DEST_ROOM       = 1;
DEST_VIDEO_CONF = 2;
NUM_DEST        = 2;
// dvd / vcr mode...
MODE_IS_DVD = 1;
MODE_IS_VCR = 2;
// relays...
rlySCREEN_UP     = 1;
rlySCREEN_DOWN   = 2;
rlySHADES_OPEN   = 3;
rlySHADES_CLOSE  = 4;
rlySHADES_MIDDLE = 5;
rlyPOD_VGA_SWT_IN_1 = 6;
rlyPOD_VGA_SWT_IN_2 = 7;
//-----------------------------------------------//
// consants for indexing volume level / mute feedback array...
VOL_PROG      = 1;
VOL_SPEECH    = 2;
VOL_POD_MIC   = 3;
VOL_WMIC      = 4;
NUM_VOL       = 4;
//=========================================================//
//=========================================================//
DEFINE_DEVICE
//---------------------------------------------------------//
// user interfaces...
dvTP = 10001:1:0; // AMX MVP-7500

// NI-3100 devices...
dvSVID_SWT  = 5001:1:0;  // Extron MMX-42SVA (4x2 svideo + audio router)
vdvSVID_SWT = 33001:1:0; // virtual device for module...
dvRGB_SWT   = 5001:2:0;  // Extron MVX-84VGA-A (8x4 rgbhv + audio router)
vdvRGB_SWT  = 33002:1:0; // virtual device for module...
dvCODEC     = 5001:3:0;  // LifeSize Room CODEC
vdvCODEC    = 33003:1:0; // virtual device for module...
dvDSP       = 5001:4:0;  // Biamp Audia DSP
vdvDSP      = 33004:1:0; // virtual device for module...
dvPLASMA    = 5001:5:0;  // Panasonic TH-103PF10UK
vdvPLASMA   = 33005:1:0; // virtual device for module...
dvLIGHTS    = 5001:6:0;  // Lutron GrafikEye
vdvLIGHTS   = 33006:1:0; // virtual device for module...
dvSCALER    = 5001:7:0;  // extron DVS-304 Scaler
vdvSCALER   = 33007:1:0; // virtual device for module...
dvDVD_VCR_IR= 5001:9:0;  // JVC SR-MV55 DVD / VCR combo
vdvDVD_VCR  = 33009:1:0; // virtual device for module...
dvSAT       = 5001:10:0; // dish network dvr
dvHDDVD     = 5001:11:0; // Sony BDP-S350 BluRay DVD player...
dvDVI_SWT   = 5001:12:0; // Extron SW2 DVI A...
dvRELAYS    = 5001:8:0;  // 
     // relay 1:  screen up
					// relay 2:  screen down
					// relay 3:  shades open
					// relay 4:  shades close
					// relay 5:  shades middle
					// relay 6:  podium vga swt input #1
					// relay 7:  podium vga swt input #2
//=========================================================//
//=========================================================//
DEFINE_TYPE
//---------------------------------------------------------//
=

//=========================================================//
//=========================================================//
DEFINE_VARIABLE
//---------------------------------------------------------//
integer current_src[NUM_DEST];      // source per destination...
integer vol_level_fb[NUM_VOL];      // volume level feedback per volume control...
integer mute_fb[NUM_VOL];           // mute feedback per volume control...
integer dvd_vcr_mode = 0;           // current dvd / vcr mode...
volatile char lutron_scene_fb[8];   // scene per grafik eye...
integer last_table_laptop = SRC_TABLE_1_RGB;   // holds last selected table laptop...
integer last_podium_src[NUM_PODIUM_LOC] = { SRC_PODIUM_1_PC, SRC_PODIUM_2_PC } // holds last selected podium source per podium location...
integer vid_conf_mute = 0;          // video conf. mixer input mute...
persistent integer podium_location; // podium location...
//=========================================================//
//=========================================================//


//=========================================================//
//=========================================================//
DEFINE_START
//---------------------------------------------------------//
// init podium_location... 
if(podium_location == 0) { podium_location = POD_LOCATION_1; }
//-----------------------------------------------//
#include 'command_const.axi';
#include 'dvd_vcr.axi';
#include 'BluRay DVD.axi';
#include 'sat.axi';
#include 'codec.axi';
#include 'dsp_rev1.axi';
#include 'switchers.axi';
#include 'scaler.axi';
#include 'monitor.axi';
#include 'screen.axi';
#include 'shades.axi';
#include 'lutron.axi';
#include 'system functions.axi';
#include 'touchpanel.axi';
//=========================================================//
//=========================================================//
define_module 'Biamp_AudiaSOLO_Comm'      dsp(vdvDSP, dvDSP);
define_module 'Panasonic TH-103PF10UK' plasma(vdvPLASMA, dvPLASMA);

//=========================================================//
//=========================================================//
DEFINE_EVENT
//---------------------------------------------------------//


//=========================================================//
//=========================================================//
DEFINE_PROGRAM
//---------------------------------------------------------//


//=========================================================//
//                     END OF PROGRAM                      //
//        DO NOT PUT ANY CODE BELOW THIS COMMENT           //
//=========================================================//


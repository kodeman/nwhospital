PROGRAM_NAME='switchers'
//=========================================================//
//=========================================================//
DEFINE_CONSTANT
//---------------------------------------------------------//
SWT_TYPE_AUDIO = 1;
SWT_TYPE_VIDEO = 2;
NUM_SWT_TYPE   = 2;
//=========================================================//
//=========================================================//
DEFINE_TYPE
//---------------------------------------------------------//


//=========================================================//
//=========================================================//
DEFINE_VARIABLE
//---------------------------------------------------------//
char rgb_swt_inputs[NUM_SRC][NUM_SWT_TYPE][2];
char svid_swt_inputs[NUM_SRC][NUM_SWT_TYPE][2];
char rgb_swt_outputs[NUM_DEST][NUM_SWT_TYPE][2];
char svid_swt_outputs[NUM_DEST][NUM_SWT_TYPE][2];

//=========================================================//
//=========================================================//
define_function do_audio_route(integer src, integer dest)
{
 send_string dvRGB_SWT,"rgb_swt_inputs[src][SWT_TYPE_AUDIO],'*',rgb_swt_outputs[dest][SWT_TYPE_AUDIO],'$'";
	send_string dvSVID_SWT,"svid_swt_inputs[src][SWT_TYPE_AUDIO],'*',svid_swt_outputs[dest][SWT_TYPE_AUDIO],'$'";
	// dvi switch...
	switch(src)
	{
	 case SRC_PODIUM_1_PC:
		case SRC_PODIUM_2_PC:
		{
		 // select input 1
			send_command dvDVI_SWT,"'sp',1";
		}
		case SRC_PODIUM_1_LAPTOP_DVI:
		case SRC_PODIUM_2_LAPTOP_DVI:
		case SRC_PODIUM_1_LAPTOP_RGB:
		case SRC_PODIUM_2_LAPTOP_RGB:
		{
		 // select input 2
			send_command dvDVI_SWT,"'sp',2";
		}
		default: {  }
	}
}
//-----------------------------------------------//
define_function do_video_route(integer src, integer dest)
{
 send_string dvRGB_SWT,"rgb_swt_inputs[src][SWT_TYPE_VIDEO],'*',rgb_swt_outputs[dest][SWT_TYPE_VIDEO],'&'";
	send_string dvSVID_SWT,"svid_swt_inputs[src][SWT_TYPE_VIDEO],'*',svid_swt_outputs[dest][SWT_TYPE_VIDEO],'%'";
	// dvi switch...
	switch(src)
	{
	 case SRC_PODIUM_1_PC:
		case SRC_PODIUM_2_PC:
		{
		 // select input 1
			send_command dvDVI_SWT,"'sp',1";
		}
		case SRC_PODIUM_1_LAPTOP_DVI:
		case SRC_PODIUM_2_LAPTOP_DVI:
		{
		 // select input 2
			send_command dvDVI_SWT,"'sp',2";
		}
		default: {  }
	}
}
//=========================================================//
//=========================================================//
DEFINE_START
//---------------------------------------------------------//
/////////////// inputs ///////////////
// rgb switch audio inputs...
rgb_swt_inputs[SRC_VID_CONF][SWT_TYPE_AUDIO]            = "'0'";
rgb_swt_inputs[SRC_PODIUM_1_PC][SWT_TYPE_AUDIO]         = "'6'";
rgb_swt_inputs[SRC_PODIUM_1_LAPTOP_DVI][SWT_TYPE_AUDIO] = "'6'";
rgb_swt_inputs[SRC_PODIUM_1_LAPTOP_RGB][SWT_TYPE_AUDIO] = "'6'";
rgb_swt_inputs[SRC_PODIUM_2_PC][SWT_TYPE_AUDIO]         = "'6'";
rgb_swt_inputs[SRC_PODIUM_2_LAPTOP_DVI][SWT_TYPE_AUDIO] = "'6'";
rgb_swt_inputs[SRC_PODIUM_2_LAPTOP_RGB][SWT_TYPE_AUDIO] = "'6'";
rgb_swt_inputs[SRC_TABLE_1_RGB][SWT_TYPE_AUDIO]         = "'3'";
rgb_swt_inputs[SRC_TABLE_2_RGB][SWT_TYPE_AUDIO]         = "'4'";
rgb_swt_inputs[SRC_TABLE_3_RGB][SWT_TYPE_AUDIO]         = "'7'";
rgb_swt_inputs[SRC_TABLE_4_RGB][SWT_TYPE_AUDIO]         = "'8'";
rgb_swt_inputs[SRC_RACK_VGA][SWT_TYPE_AUDIO]            = "'5'";
rgb_swt_inputs[SRC_RACK_SVIDEO][SWT_TYPE_AUDIO]         = "'0'";
rgb_swt_inputs[SRC_DVD_VCR][SWT_TYPE_AUDIO]             = "'0'";
rgb_swt_inputs[SRC_HDDVD][SWT_TYPE_AUDIO]               = "'0'";
rgb_swt_inputs[SRC_SAT][SWT_TYPE_AUDIO]                 = "'0'";
rgb_swt_inputs[SRC_CAM_2][SWT_TYPE_AUDIO]               = "'0'";
rgb_swt_inputs[SRC_CAM_1][SWT_TYPE_AUDIO]               = "'0'";
rgb_swt_inputs[SRC_SYSTEM_OFF][SWT_TYPE_AUDIO]          = "'0'";
// rgb switch video inputs...
rgb_swt_inputs[SRC_VID_CONF][SWT_TYPE_VIDEO]            = "'1'";
rgb_swt_inputs[SRC_PODIUM_1_PC][SWT_TYPE_VIDEO]         = "'0'";
rgb_swt_inputs[SRC_PODIUM_1_LAPTOP_DVI][SWT_TYPE_VIDEO] = "'0'";
rgb_swt_inputs[SRC_PODIUM_1_LAPTOP_RGB][SWT_TYPE_VIDEO] = "'6'";
rgb_swt_inputs[SRC_PODIUM_2_PC][SWT_TYPE_VIDEO]         = "'0'";
rgb_swt_inputs[SRC_PODIUM_2_LAPTOP_DVI][SWT_TYPE_VIDEO] = "'0'";
rgb_swt_inputs[SRC_PODIUM_2_LAPTOP_RGB][SWT_TYPE_VIDEO] = "'6'";
rgb_swt_inputs[SRC_TABLE_1_RGB][SWT_TYPE_VIDEO]         = "'3'";
rgb_swt_inputs[SRC_TABLE_2_RGB][SWT_TYPE_VIDEO]         = "'4'";
rgb_swt_inputs[SRC_TABLE_3_RGB][SWT_TYPE_VIDEO]         = "'7'";
rgb_swt_inputs[SRC_TABLE_4_RGB][SWT_TYPE_VIDEO]         = "'8'";
rgb_swt_inputs[SRC_RACK_VGA][SWT_TYPE_VIDEO]            = "'5'";
rgb_swt_inputs[SRC_RACK_SVIDEO][SWT_TYPE_VIDEO]         = "'6'";
rgb_swt_inputs[SRC_DVD_VCR][SWT_TYPE_VIDEO]             = "'6'";
rgb_swt_inputs[SRC_HDDVD][SWT_TYPE_VIDEO]               = "'0'";
rgb_swt_inputs[SRC_SAT][SWT_TYPE_VIDEO]                 = "'0'";
rgb_swt_inputs[SRC_CAM_1][SWT_TYPE_VIDEO]               = "'0'";
rgb_swt_inputs[SRC_CAM_2][SWT_TYPE_VIDEO]               = "'0'";
rgb_swt_inputs[SRC_SYSTEM_OFF][SWT_TYPE_VIDEO]          = "'0'";
// svideo switch audio inputs...
svid_swt_inputs[SRC_VID_CONF][SWT_TYPE_AUDIO]            = "'1'";
svid_swt_inputs[SRC_PODIUM_1_PC][SWT_TYPE_AUDIO]         = "'0'";
svid_swt_inputs[SRC_PODIUM_1_LAPTOP_DVI][SWT_TYPE_AUDIO] = "'0'";
svid_swt_inputs[SRC_PODIUM_1_LAPTOP_RGB][SWT_TYPE_AUDIO] = "'0'";
svid_swt_inputs[SRC_PODIUM_2_PC][SWT_TYPE_AUDIO]         = "'0'";
svid_swt_inputs[SRC_PODIUM_2_LAPTOP_DVI][SWT_TYPE_AUDIO] = "'0'";
svid_swt_inputs[SRC_PODIUM_2_LAPTOP_RGB][SWT_TYPE_AUDIO] = "'0'";
svid_swt_inputs[SRC_TABLE_1_RGB][SWT_TYPE_AUDIO]         = "'0'";
svid_swt_inputs[SRC_TABLE_2_RGB][SWT_TYPE_AUDIO]         = "'0'";
svid_swt_inputs[SRC_TABLE_3_RGB][SWT_TYPE_AUDIO]         = "'0'";
svid_swt_inputs[SRC_TABLE_4_RGB][SWT_TYPE_AUDIO]         = "'0'";
svid_swt_inputs[SRC_RACK_VGA][SWT_TYPE_AUDIO]            = "'0'";
svid_swt_inputs[SRC_RACK_SVIDEO][SWT_TYPE_AUDIO]         = "'2'";
svid_swt_inputs[SRC_DVD_VCR][SWT_TYPE_AUDIO]             = "'3'";
svid_swt_inputs[SRC_HDDVD][SWT_TYPE_AUDIO]               = "'1'";
svid_swt_inputs[SRC_SAT][SWT_TYPE_AUDIO]                 = "'4'";
svid_swt_inputs[SRC_CAM_1][SWT_TYPE_AUDIO]               = "'0'";
svid_swt_inputs[SRC_CAM_2][SWT_TYPE_AUDIO]               = "'0'";
svid_swt_inputs[SRC_SYSTEM_OFF][SWT_TYPE_AUDIO]          = "'0'";
// svideo switch video inputs...
svid_swt_inputs[SRC_VID_CONF][SWT_TYPE_VIDEO]            = "'0'";
svid_swt_inputs[SRC_PODIUM_1_PC][SWT_TYPE_VIDEO]         = "'0'";
svid_swt_inputs[SRC_PODIUM_1_LAPTOP_DVI][SWT_TYPE_VIDEO] = "'0'";
svid_swt_inputs[SRC_PODIUM_1_LAPTOP_RGB][SWT_TYPE_VIDEO] = "'0'";
svid_swt_inputs[SRC_PODIUM_2_PC][SWT_TYPE_VIDEO]         = "'0'";
svid_swt_inputs[SRC_PODIUM_2_LAPTOP_DVI][SWT_TYPE_VIDEO] = "'0'";
svid_swt_inputs[SRC_PODIUM_2_LAPTOP_RGB][SWT_TYPE_VIDEO] = "'0'";
svid_swt_inputs[SRC_TABLE_1_RGB][SWT_TYPE_VIDEO]         = "'0'";
svid_swt_inputs[SRC_TABLE_2_RGB][SWT_TYPE_VIDEO]         = "'0'";
svid_swt_inputs[SRC_TABLE_3_RGB][SWT_TYPE_VIDEO]         = "'0'";
svid_swt_inputs[SRC_TABLE_4_RGB][SWT_TYPE_VIDEO]         = "'0'";
svid_swt_inputs[SRC_RACK_VGA][SWT_TYPE_VIDEO]            = "'0'";
svid_swt_inputs[SRC_RACK_SVIDEO][SWT_TYPE_VIDEO]         = "'2'";
svid_swt_inputs[SRC_DVD_VCR][SWT_TYPE_VIDEO]             = "'3'";
svid_swt_inputs[SRC_HDDVD][SWT_TYPE_VIDEO]               = "'0'";
svid_swt_inputs[SRC_SAT][SWT_TYPE_VIDEO]                 = "'0'";
svid_swt_inputs[SRC_CAM_1][SWT_TYPE_VIDEO]               = "'0'";
svid_swt_inputs[SRC_CAM_2][SWT_TYPE_VIDEO]               = "'0'";
svid_swt_inputs[SRC_SYSTEM_OFF][SWT_TYPE_VIDEO]          = "'0'";
/////////////// outputs ///////////////
// rgb switch audio outputs...
rgb_swt_outputs[DEST_ROOM][SWT_TYPE_AUDIO]       = "'1'";
rgb_swt_outputs[DEST_VIDEO_CONF][SWT_TYPE_AUDIO] = "'1'";
// rgb switch video outputs...
rgb_swt_outputs[DEST_ROOM][SWT_TYPE_VIDEO]       = "'2'";
rgb_swt_outputs[DEST_VIDEO_CONF][SWT_TYPE_VIDEO] = "'1'";
// svideo switch audio outputs...
svid_swt_outputs[DEST_ROOM][SWT_TYPE_AUDIO]       = "'1'";
svid_swt_outputs[DEST_VIDEO_CONF][SWT_TYPE_AUDIO] = "'1'";
// svideo switch video outputs...
svid_swt_outputs[DEST_ROOM][SWT_TYPE_VIDEO]       = "'2'";
svid_swt_outputs[DEST_VIDEO_CONF][SWT_TYPE_VIDEO] = "'1'";
//=========================================================//
//=========================================================//
DEFINE_EVENT
//---------------------------------------------------------//
data_event[dvRGB_SWT]
data_event[dvSVID_SWT]
{
 online: 
	{
	 send_command data.device,"'SET BAUD 9600,N,8,1,485 DISABLE'";
	}
}

//=========================================================//
//=========================================================//
DEFINE_PROGRAM
//---------------------------------------------------------//
// podium vga switcher...
if(current_src[DEST_ROOM] == SRC_PODIUM_1_PC || current_src[DEST_VIDEO_CONF] == SRC_PODIUM_1_PC)
{
 on[dvRELAYS,rlyPOD_VGA_SWT_IN_1]
	off[dvRELAYS,rlyPOD_VGA_SWT_IN_2]
}
else if(current_src[DEST_ROOM] == SRC_PODIUM_1_LAPTOP_DVI || 
        current_src[DEST_ROOM] == SRC_PODIUM_1_LAPTOP_RGB ||
        current_src[DEST_VIDEO_CONF] == SRC_PODIUM_1_LAPTOP_DVI || 
								current_src[DEST_VIDEO_CONF] == SRC_PODIUM_1_LAPTOP_RGB )
{
 off[dvRELAYS,rlyPOD_VGA_SWT_IN_1]
	on[dvRELAYS,rlyPOD_VGA_SWT_IN_2]
}
//=========================================================//
//                     END OF PROGRAM                      //
//        DO NOT PUT ANY CODE BELOW THIS COMMENT           //
//=========================================================//
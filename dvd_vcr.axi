PROGRAM_NAME='dvd_vcr'
//=========================================================//
//=========================================================//
DEFINE_CONSTANT
//---------------------------------------------------------//


//=========================================================//
//=========================================================//
DEFINE_TYPE
//---------------------------------------------------------//


//=========================================================//
//=========================================================//
DEFINE_VARIABLE
//---------------------------------------------------------//


//=========================================================//
//=========================================================//
//define_function do_dvd_vcr_select(char cmd[])
//{
 //switch(cmd)
 //{
	//case 'DVD': { send_command vdvDVD_VCR,"'sp',82"; }
	//case 'VCR': { send_command vdvDVD_VCR,"'sp',82"; }
	//default: {  }
 //}
//}
//-----------------------------------------------//
define_function do_dvd_vcr_cmd(char cmd[])
{
 switch(cmd)
 {
	 case cmdPOWER_ON:   { send_command dvDVD_VCR_IR,"'sp'"; }
	 case cmdPOWER_OFF:  { send_command dvDVD_VCR_IR,"'sp'"; }
	 case cmdPLAY:       { send_command dvDVD_VCR_IR,"'sp',1"; }
	 case cmdSTOP:       { send_command dvDVD_VCR_IR,"'sp',2"; }
	 case cmdPAUSE:      { send_command dvDVD_VCR_IR,"'sp',3"; }
	 case cmdTRACK_FWD:  { send_command dvDVD_VCR_IR,"'sp',4"; }
	 case cmdTRACK_BACK: { send_command dvDVD_VCR_IR,"'sp',5"; }
	 case cmdFFWD:       { send_command dvDVD_VCR_IR,"'sp',6"; }
	 case cmdREW:        { send_command dvDVD_VCR_IR,"'sp',7"; }
	 case cmdTOP_MENU:   { send_command dvDVD_VCR_IR,"'sp',91"; }
  case cmdSETUP:      { send_command dvDVD_VCR_IR,"'sp',92"; }
  case cmdMENU:       { send_command dvDVD_VCR_IR,"'sp',85"; }
  case cmdCUR_UP:     { send_command dvDVD_VCR_IR,"'sp',86"; }
  case cmdCUR_DOWN:   { send_command dvDVD_VCR_IR,"'sp',87"; }
  case cmdCUR_LEFT:   { send_command dvDVD_VCR_IR,"'sp',88"; }
  case cmdCUR_RIGHT:  { send_command dvDVD_VCR_IR,"'sp',89"; }
  case cmdSELECT:     { send_command dvDVD_VCR_IR,"'sp',90"; }
	 case cmdSUBTITLE:   { send_command dvDVD_VCR_IR,"'sp',101"; }
	 case cmdRETURN:     { send_command dvDVD_VCR_IR,"'sp',93"; }
	 case cmdDVD_VCR:    { send_command dvDVD_VCR_IR,"'sp',82"; }
	 default: {  }
 }
}
//=========================================================//
//=========================================================//
DEFINE_START
//---------------------------------------------------------//


//=========================================================//
//=========================================================//
DEFINE_EVENT
//---------------------------------------------------------//
data_event[dvDVD_VCR_IR]
{
 online:
 {
  send_command data.device,"'CTON',2";
	 send_command data.device,"'CTOF',4";
	 send_command data.device,"'SET MODE IR'";
	 send_command data.device,"'CARON'";
 }
}

//=========================================================//
//=========================================================//
DEFINE_PROGRAM
//---------------------------------------------------------//


//=========================================================//
//                     END OF PROGRAM                      //
//        DO NOT PUT ANY CODE BELOW THIS COMMENT           //
//=========================================================//
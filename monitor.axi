PROGRAM_NAME='monitor'
//=========================================================//
//=========================================================//
DEFINE_CONSTANT
//---------------------------------------------------------//

//=========================================================//
//=========================================================//
DEFINE_TYPE
//---------------------------------------------------------//


//=========================================================//
//=========================================================//
DEFINE_VARIABLE
//---------------------------------------------------------//


//=========================================================//
//=========================================================//
define_function do_mon_cmd(char cmd[])
{
 switch(cmd)
	{
	 case cmdPOWER_ON:  { send_command vdvPLASMA,"'POWER=1'"; }
		case cmdPOWER_OFF: { send_command vdvPLASMA,"'POWER=0'"; }
		default: {  }
	}
}
//-----------------------------------------------//
define_function do_mon_input(integer src)
{
 do_scaler_input(src);
 switch(src)
	{
  case SRC_VID_CONF           : { send_command vdvPLASMA,"'INPUT=PC'"; }
  case SRC_PODIUM_1_PC        : { send_command vdvPLASMA,"'INPUT=SLOT_2A'"; }
  case SRC_PODIUM_1_LAPTOP_DVI: { send_command vdvPLASMA,"'INPUT=SLOT_2B'"; }
  case SRC_PODIUM_1_LAPTOP_RGB: { send_command vdvPLASMA,"'INPUT=PC'"; }
  case SRC_PODIUM_2_PC        : { send_command vdvPLASMA,"'INPUT=SLOT_2A'"; }
  case SRC_PODIUM_2_LAPTOP_DVI: { send_command vdvPLASMA,"'INPUT=SLOT_2B'"; }
  case SRC_PODIUM_2_LAPTOP_RGB: { send_command vdvPLASMA,"'INPUT=PC'"; }
  case SRC_TABLE_1_RGB        : { send_command vdvPLASMA,"'INPUT=PC'"; }
  case SRC_TABLE_2_RGB        : { send_command vdvPLASMA,"'INPUT=PC'"; }
  case SRC_TABLE_3_RGB        : { send_command vdvPLASMA,"'INPUT=PC'"; }
  case SRC_TABLE_4_RGB        : { send_command vdvPLASMA,"'INPUT=PC'"; }
  case SRC_RACK_VGA           : { send_command vdvPLASMA,"'INPUT=PC'"; }
  case SRC_RACK_SVIDEO        : { send_command vdvPLASMA,"'INPUT=PC'"; }
  case SRC_DVD_VCR            : { send_command vdvPLASMA,"'INPUT=PC'"; }
  case SRC_HDDVD              : { send_command vdvPLASMA,"'INPUT=SLOT_1B'"; }
  case SRC_SAT                : { send_command vdvPLASMA,"'INPUT=SLOT_1A'"; }
  case SRC_CAM_1              : { send_command vdvPLASMA,"'INPUT=PC'"; }
  case SRC_CAM_2              : { send_command vdvPLASMA,"'INPUT=PC'"; }
  case SRC_SYSTEM_OFF         : {  }
		default: {  }
	}
}
//-----------------------------------------------//
//=========================================================//
//=========================================================//
DEFINE_START
//---------------------------------------------------------//


//=========================================================//
//=========================================================//
DEFINE_EVENT
//---------------------------------------------------------//


//=========================================================//
//=========================================================//
DEFINE_PROGRAM
//---------------------------------------------------------//


//=========================================================//
//                     END OF PROGRAM                      //
//        DO NOT PUT ANY CODE BELOW THIS COMMENT           //
//=========================================================//
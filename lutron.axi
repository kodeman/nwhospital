PROGRAM_NAME='lutron'
//=========================================================//
//=========================================================//
DEFINE_CONSTANT
//---------------------------------------------------------//

//=========================================================//
//=========================================================//
DEFINE_TYPE
//---------------------------------------------------------//


//=========================================================//
//=========================================================//
DEFINE_VARIABLE
//---------------------------------------------------------//
volatile char lutron_buf[200]; // buffer for strings from lutron...

//=========================================================//
//=========================================================//
define_function parse_lutron_buffer()
{
 stack_var char rtn_msg[20];
	stack_var char junk[20];
	stack_var integer i; // counter
	
 if(find_string(lutron_buf, "$0D,$0A", 1)) // if we find a full message
	{
	 rtn_msg = remove_string(lutron_buf, "$0D,$0A", 1); // get the message
		if(find_string(rtn_msg, "'~:ss '", 1)) // if the message is a scene message...
		{
		 junk = remove_string(rtn_msg, "'~:ss '", 1); // get rid of the first part of the string.
			
			for(i = 1; i <= 8; i++) // for a possible of 8 units...
			{
			 if(rtn_msg[i] <> "'M'")
				{
				 lutron_scene_fb[i] = atoi(rtn_msg[i]);
				}
				else
				{
				 lutron_scene_fb[i] = rtn_msg[i];
				}
			}
		}
	}
}
//-----------------------------------------------//
define_function lights_recall_preset(integer preset_num)
{
 switch(preset_num)
	{
	 case 0:  { send_string dvLIGHTS,"':A01',$0D"; }
		case 1:  { send_string dvLIGHTS,"':A11',$0D"; }
		case 2:  { send_string dvLIGHTS,"':A21',$0D"; }
		case 3:  { send_string dvLIGHTS,"':A31',$0D"; }
		case 4:  { send_string dvLIGHTS,"':A41',$0D"; }
		case 5:  { send_string dvLIGHTS,"':A51',$0D"; }
		case 6:  { send_string dvLIGHTS,"':A61',$0D"; }
		case 7:  { send_string dvLIGHTS,"':A71',$0D"; }
		case 8:  { send_string dvLIGHTS,"':A81',$0D"; }
		case 9:  { send_string dvLIGHTS,"':A91',$0D"; }
		case 10: { send_string dvLIGHTS,"':AA1',$0D"; }
		case 11: { send_string dvLIGHTS,"':AB1',$0D"; }
		case 12: { send_string dvLIGHTS,"':AC1',$0D"; }
		case 13: { send_string dvLIGHTS,"':AD1',$0D"; }
		case 14: { send_string dvLIGHTS,"':AE1',$0D"; }
		case 15: { send_string dvLIGHTS,"':AF1',$0D"; }
	}
}
define_function do_lights_for_src(integer src)
{
 switch(src)
 {
  case SRC_VID_CONF: { lights_recall_preset(2); }
	default: {  }
 }
}
//=========================================================//
//=========================================================//
DEFINE_START
//---------------------------------------------------------//
create_buffer dvLights,lutron_buf;
clear_buffer lutron_buf;


//=========================================================//
//=========================================================//
DEFINE_EVENT
//---------------------------------------------------------//
data_event[dvLights]
{
 string:
	{
	
	}
 online:
	{
	 send_command data.device,"'SET BAUD 9600,N,8,1,485 DISABLE'";
	}
}

//=========================================================//
//=========================================================//
DEFINE_PROGRAM
//---------------------------------------------------------//
parse_lutron_buffer();

//=========================================================//
//                     END OF PROGRAM                      //
//        DO NOT PUT ANY CODE BELOW THIS COMMENT           //
//=========================================================//
PROGRAM_NAME='system functions'
//=========================================================//
//=========================================================//
DEFINE_CONSTANT
//---------------------------------------------------------//


//=========================================================//
//=========================================================//
DEFINE_TYPE
//---------------------------------------------------------//


//=========================================================//
//=========================================================//
DEFINE_VARIABLE
//---------------------------------------------------------//


//=========================================================//
//=========================================================//
define_function do_source_select(integer src, integer dest)
{
 switch(dest)
	{
	 case DEST_ROOM:
		{
		 // set current src...
	  current_src[dest] = src;
			// page flips...
			do_page_flips();
   // route audio...
	  do_audio_route(src, dest);
	  // route video...
	  do_video_route(src, dest);
	  // screen down...
	  //do_screen(cmdCUR_DOWN);
	  // projector power on...
	  //do_proj_cmd(cmdPOWER_ON);
			// monitor power on...
	  do_mon_cmd(cmdPOWER_ON);
	  // projector input select...
   //do_proj_input(src);
			// monitor input select...
   do_mon_input(src);
   // dsp???
	   // shades preset...
		 //do_shades_for_src(src); removed 6/29/2009
		 // lights preset...
		 do_lights_for_src(src);
			// last table laptop
			if(src == SRC_TABLE_1_RGB || src == SRC_TABLE_2_RGB || src == SRC_TABLE_3_RGB || src == SRC_TABLE_4_RGB)
			{
			 last_table_laptop = src;
			}
			// last podium sources...
			if(src == SRC_PODIUM_1_PC || src == SRC_PODIUM_1_LAPTOP_DVI || src == SRC_PODIUM_1_LAPTOP_RGB)
			{
			 last_podium_src[POD_LOCATION_1] = src;
			}
			if(src == SRC_PODIUM_2_PC || src == SRC_PODIUM_2_LAPTOP_DVI || src == SRC_PODIUM_2_LAPTOP_RGB)
			{
			 last_podium_src[POD_LOCATION_2] = src;
			}
			if(src == SRC_VID_CONF) 
			{ 
			 // un-mute codec to speakers...
	   do_vid_conf_mixer_mute(0);
			}
			else
			{
			 // mute codec to speakers...
	   //do_vid_conf_mixer_mute(1);
			}
		}
		case DEST_VIDEO_CONF:
		{
		 // set current src...
	  current_src[dest] = src;
			// page flips...
			do_page_flips();
   // route audio...
	  do_audio_route(src, dest);
	  // route video...
	  do_video_route(src, dest);
   //  codec
			do_src_for_codec(src);
			// dsp???
			
		}
		default: {  }
		// dvd vcr power...
		if(src == SRC_DVD_VCR)
		{
		 do_dvd_vcr_cmd(cmdPOWER_ON);
		}
	}
}
//-----------------------------------------------//
define_function do_system_off()
{
 // set current src...
	current_src[DEST_ROOM] = SRC_SYSTEM_OFF;
 // screen up...
	//do_screen(cmdCUR_UP);
	// projector power off...
	//do_proj_cmd(cmdPOWER_OFF);
	// monitor power off...
	do_mon_cmd(cmdPOWER_OFF);
	// audio route
	do_audio_route(SRC_SYSTEM_OFF, DEST_ROOM);
	// dvd vcr power off...
	do_dvd_vcr_cmd(cmdPOWER_OFF);
	// mute codec to speakers...
	do_vid_conf_mixer_mute(1);
}
//-----------------------------------------------//
define_function do_xport(integer src, char cmd[])
{
 switch(src)
	{
	 case SRC_DVD_VCR : { do_dvd_vcr_cmd(cmd); }
		case SRC_HDDVD   : { do_hddvd_cmd(cmd); }
		case SRC_SAT     : { do_sat_cmd(cmd); }
	 case SRC_VID_CONF: 
	 {
	  switch(cmd)
		 {
 		 case cmdCUR_UP:    { do_codec_cmd(CODEC_CMD_CUR_UP); }
		  case cmdCUR_DOWN:  { do_codec_cmd(CODEC_CMD_CUR_DOWN); }
		  case cmdCUR_LEFT:  { do_codec_cmd(CODEC_CMD_CUR_LEFT); }
		  case cmdCUR_RIGHT: { do_codec_cmd(CODEC_CMD_CUR_RIGHT); }
		  case cmdSELECT:    { do_codec_cmd(CODEC_CMD_OK); }
		  default: {  }
		 }
	 }
		default: {  }
	}
}
//-----------------------------------------------//
//=========================================================//
//=========================================================//
DEFINE_START
//---------------------------------------------------------//


//=========================================================//
//=========================================================//
DEFINE_EVENT
//---------------------------------------------------------//


//=========================================================//
//=========================================================//
DEFINE_PROGRAM
//---------------------------------------------------------//


//=========================================================//
//                     END OF PROGRAM                      //
//        DO NOT PUT ANY CODE BELOW THIS COMMENT           //
//=========================================================//
PROGRAM_NAME='command_const'
//=========================================================//
//=========================================================//
DEFINE_CONSTANT
//---------------------------------------------------------//
#if_not_defined cmd_const
#define cmd_const

cmdPOWER_ON     = 'POWER=1';
cmdPOWER_OFF    = 'POWER=0';
cmdVOL_UP       = 'VOLUME=+';
cmdVOL_DOWN     = 'VOLUME=-';
cmdMUTE_ON      = 'MUTE=1';
cmdMUTE_OFF     = 'MUTE=0';
cmdMUTE_TOGGLE  = 'MUTE=T';
cmdSHADES_OPEN  = 'OPEN';
cmdSHADES_CLOSE = 'CLOSE';
cmdSHADES_MIDDLE= 'MIDDLE';
cmdSHADES_STOP  = 'STOP';
cmdNUM_0        = 'NUM=0';
cmdNUM_1        = 'NUM=1';
cmdNUM_2        = 'NUM=2';
cmdNUM_3        = 'NUM=3';
cmdNUM_4        = 'NUM=4';
cmdNUM_5        = 'NUM=5';
cmdNUM_6        = 'NUM=6';
cmdNUM_7        = 'NUM=7';
cmdNUM_8        = 'NUM=8';
cmdNUM_9        = 'NUM=9';
cmdNUM_PLUS_10  = 'NUM=+10';
cmdCHAN_UP      = 'CHAN=+';
cmdCHAN_DOWN    = 'CHAN=-';
cmdGOTO_CHAN    = 'CHAN=';
cmdPLAY         = 'PLAY';
cmdSTOP         = 'STOP';
cmdPAUSE        = 'PAUSE';
cmdTRACK_FWD    = 'TRACK_FWD';
cmdTRACK_BACK   = 'TRACK_BACK';
cmdFFWD         = 'FFWD';
cmdREW          = 'REW';
cmdRECORD       = 'RECORD';
cmdTOP_MENU     = 'TOP_MENU';
cmdSETUP        = 'SETUP';
cmdMENU         = 'MENU';
cmdCUR_UP       = 'CUR_UP';
cmdCUR_DOWN     = 'CUR_DOWN';
cmdCUR_LEFT     = 'CUR_LEFT';
cmdCUR_RIGHT    = 'CUR_RIGHT';
cmdSELECT       = 'SELECT';
cmdGUIDE        = 'GUIDE';
cmdDISPLAY      = 'DISPLAY';
cmdCLEAR        = 'CLEAR';
cmdRETURN       = 'RETURN';
cmdSUBTITLE     = 'SUBTITLE';
cmdRED          = 'RED';
cmdGREEN        = 'GREEN';
cmdBLUE         = 'BLUE';
cmdYELLOW       = 'YELLOW';
cmdAM_FM        = 'AM_FB';
cmdZOOM_IN      = 'ZOOM+';
cmdZOOM_OUT     = 'ZOOM-';     
cmdDVD_VCR      = 'DVD_VCR';
cmdPAGE_UP      = 'PAGE_UP';
cmdPAGE_DOWN    = 'PAGE_DOWN';
cmdRECALL       = 'RECALL';
cmdCANCEL       = 'CANCEL';
cmdLIVE_TV      = 'LIVE_TV';
cmdDVR          = 'DVR';
cmdSTAR         = '*';
cmdPOUND        = '#';
#end_if

//=========================================================//
//                     END OF PROGRAM                      //
//        DO NOT PUT ANY CODE BELOW THIS COMMENT           //
//=========================================================//
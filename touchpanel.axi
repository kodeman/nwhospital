PROGRAM_NAME='touchpanel'
//=========================================================//
//=========================================================//
DEFINE_CONSTANT
//---------------------------------------------------------//
// options page constants...
OPTIONS_PG_LIGHTS_SHADES = 1;
OPTIONS_PG_VOL_CTRLS     = 2;
// codec page constants...
CODEC_PG_SRC_CTRLS   = 1;
CODEC_PG_XPORT_CTRLS = 2;
//-----------------------------------------------//
// source select buttons...
btnSRC_VID_CONF          = 1;
btnSRC_PODIUM            = 2;
btnSRC_PODIUM_PC         = 3;
btnSRC_PODIUM_LAPTOP_DVI = 4;
btnSRC_PODIUM_LAPTOP_RGB = 5;
btnSRC_TABLE_LAPTOPS     = 6;
btnSRC_TABLE_1_RGB       = 7;
btnSRC_TABLE_2_RGB       = 8;
btnSRC_TABLE_3_RGB       = 9;
btnSRC_TABLE_4_RGB       = 10;
btnSRC_RACK_VGA          = 11;
btnSRC_RACK_SVIDEO       = 12;
btnSRC_DVD_VCR           = 13;
btnSRC_HDDVD             = 14;
btnSRC_SAT               = 15;
btnSRC_SYSTEM_OFF        = 16;
btnSYSTEM_OFF            = 17;
//-----------------------------------------------//
// options page buttons...
btnOPTIONS_PG_VOL           = 21;
btnOPTIONS_PG_LIGHTS_SHADES = 22;
//-----------------------------------------------//
// volume control buttons...
btnPROG_VOL_UP         = 51;
btnPROG_VOL_DOWN       = 52;
btnPROG_MUTE           = 53;
btnSPPECH_VOL_UP       = 54;
btnSPEECH_VOL_DOWN     = 55;
btnSPEECH_MUTE         = 56;
btnPODIUM_MIC_VOL_UP   = 57;
btnPODIUM_MIC_VOL_DOWN = 58;
btnPODIUM_MIC_MUTE     = 59;
btnWMIC_VOL_UP         = 60;
btnWMIC_VOL_DOWN       = 61;
btnWMIC_MUTE           = 62;
btnVID_CONF_MUTE_TOGGLE= 66;
//-----------------------------------------------//
// podium location buttons...
btnPOD_LOCATION_1 = 71;
btnPOD_LOCATION_2 = 72;
//-----------------------------------------------//
// refresh page flips button...
btnREFRESH_PG_FLIPS = 76;
//-----------------------------------------------//
// projector blanking buttons...
btnPROJ_MUTE_ON  = 81;
btnPROJ_MUTE_OFF = 82;
//-----------------------------------------------//
// codec page buttons...
btnCODEC_PG_XPORT_CTRLS = 91;
btnCODEC_PG_SRC_CTRLS   = 92;
// codec source select butons...
btnCODEC_SRC_PODIUM_LAPTOP = 100;
btnCODEC_SRC_CAM_1       = 101;
btnCODEC_SRC_CAM_2       = 102;
btnCODEC_SRC_PODIUM_PC   = 103;
btnCODEC_SRC_TABLE_1_RGB = 104;
btnCODEC_SRC_TABLE_2_RGB = 105;
btnCODEC_SRC_TABLE_3_RGB = 106;
btnCODEC_SRC_TABLE_4_RGB = 107;
btnCODEC_SRC_RACK_VGA    = 108;
btnCODEC_SRC_RACK_SVIDEO = 109;
btnCODEC_SRC_DVD_VCR     = 110;
// codec control buttons...
btnCODEC_CALL      = 111;
btnCODEC_TRIANGLE  = 112;
btnCODEC_SQUARE    = 113;
btnCODEC_CIRCLE    = 114;
btnCODEC_BACK      = 115;
btnCODEC_OK        = 116;
btnCODEC_CUR_UP    = 117;
btnCODEC_CUR_DOWN  = 118;
btnCODEC_CUR_LEFT  = 119;
btnCODEC_CUR_RIGHT = 120;
btnCODEC_VOL_UP    = 121;
btnCODEC_VOL_DOWN  = 122;
btnCODEC_MUTE      = 123;
btnCODEC_ZOOM_IN   = 124;
btnCODEC_ZOOM_OUT  = 125;
btnCODEC_NEAR      = 126;
btnCODEC_FAR       = 127;
btnCODEC_0         = 128;
btnCODEC_1         = 129;
btnCODEC_2         = 130;
btnCODEC_3         = 131;
btnCODEC_4         = 132;
btnCODEC_5         = 133;
btnCODEC_6         = 134;
btnCODEC_7         = 135;
btnCODEC_8         = 136;
btnCODEC_9         = 137;
btnCODEC_POUND     = 138;
btnCODEC_STAR      = 139;
btnCODEC_CAM_UP       = 141;
btnCODEC_CAM_DOWN     = 142;
btnCODEC_CAM_LEFT     = 143;
btnCODEC_CAM_RIGHT    = 144;
btnCODEC_CAM_ZOOM_IN  = 145;
btnCODEC_CAM_ZOOM_OUT = 146;
btnCODEC_HANGUP       = 147;
//-----------------------------------------------//
// dvd / vcr transport buttons...
btnPLAY       = 201;
btnSTOP       = 202;
btnPAUSE      = 203;
btnTRACK_FWD  = 204;
btnTRACK_BACK = 205;
btnFFWD       = 206;
btnREW        = 207;
btnMENU       = 208;
btnCUR_UP     = 209;
btnCUR_DOWN   = 210;
btnCUR_LEFT   = 211;
btnCUR_RIGHT  = 212;
btnENTER      = 213;
btnSUBTITLE   = 215;
btnRETURN     = 216;
btnDVD_VCR    = 217;
btnPG_UP      = 218;
btnPG_DOWN    = 219;
btnGUIDE      = 220;
btnRECALL     = 221;
btnCANCEL     = 222;
btnLIVE_TV    = 223;
btnDVR        = 224;
btnRECORD     = 225;
btnNUM_1      = 231;
btnNUM_2      = 232;
btnNUM_3      = 233;
btnNUM_4      = 234;
btnNUM_5      = 235;
btnNUM_6      = 236;
btnNUM_7      = 237;
btnNUM_8      = 238;
btnNUM_9      = 239;
btnNUM_0      = 240;
btnSTAR       = 241;
btnPOUND      = 242;
btnRED        = 243;
btnGREEN      = 244;
btnBLUE       = 245;
btnYELLOW     = 246;
//-----------------------------------------------//
// external buttons...
btnEXT_CUR_UP    = 251;
btnEXT_CUR_DOWN  = 252;
btnEXT_CUR_LEFT  = 253;
btnEXT_CUR_RIGHT = 254;
btnEXT_ENTER     = 255;
//-----------------------------------------------//
// lighting buttons...
btnLIGHTS_OFF      = 301;
btnLIGHTS_PRESET_1 = 302;
btnLIGHTS_PRESET_2 = 303;
btnLIGHTS_PRESET_3 = 304;
btnLIGHTS_PRESET_4 = 305;
// shades buttons...
btnSHADES_OPEN  = 321;
btnSHADES_CLOSE = 322;
btnSHADES_MIDDLE= 323;
// screen buttons...
btnSCREEN_UP   = 341;
btnSCREEN_DOWN = 342;
//-----------------------------------------------//
// volume levels...
lvlPROG_VOL    = 1;
lvlSPEECH_VOL  = 2;
lvlPOD_MIC_VOL = 3;
lvlWMIC_VOL    = 4;
//=========================================================//
//=========================================================//
DEFINE_TYPE
//---------------------------------------------------------//


//=========================================================//
//=========================================================//
DEFINE_VARIABLE
//---------------------------------------------------------//
integer tp_options_pg = 0;
integer tp_codec_pg   = CODEC_PG_XPORT_CTRLS;
integer flash = 0;
//=========================================================//
//=========================================================//
define_function do_page_flips()
{
 // kill all pop-up pages...
 send_command dvTP,"'@PPX'";
	// what is the current source?
	switch(current_src[DEST_ROOM])
	{
		case SRC_VID_CONF   : 
		{
   send_command dvTP,"'@PPN-codec controls select'";
			switch(tp_codec_pg)
			{
			 case CODEC_PG_SRC_CTRLS:   
				{
				 send_command dvTP,"'@PPN-codec source controls'"; 
					switch(current_src[DEST_VIDEO_CONF])
			  {
				  case SRC_CAM_1              : { send_command dvTP,"'@PPN-codec cam controls'"; }
				  case SRC_CAM_2              : { send_command dvTP,"'@PPN-codec cam controls'"; }
				  case SRC_PODIUM_1_PC        : { send_command dvTP,"''"; }
						case SRC_PODIUM_1_LAPTOP_DVI: { send_command dvTP,"''"; }
						case SRC_PODIUM_1_LAPTOP_RGB: { send_command dvTP,"''"; }
						case SRC_PODIUM_2_PC        : { send_command dvTP,"''"; }
						case SRC_PODIUM_2_LAPTOP_DVI: { send_command dvTP,"''"; }
						case SRC_PODIUM_2_LAPTOP_RGB: { send_command dvTP,"''"; }
		    case SRC_TABLE_1_RGB        : { send_command dvTP,"''"; }
		    case SRC_TABLE_2_RGB        : { send_command dvTP,"''"; }
						case SRC_TABLE_3_RGB        : { send_command dvTP,"''"; }
		    case SRC_TABLE_4_RGB        : { send_command dvTP,"''"; }
		    case SRC_RACK_VGA           : { send_command dvTP,"''"; }
		    case SRC_RACK_SVIDEO        : { send_command dvTP,"''"; }
						case SRC_HDDVD              : { send_command dvTP,"'@PPN-codec hddvd controls'"; }
						case SRC_SAT                : { send_command dvTP,"'@PPN-codec sat controls'"; }
		    case SRC_DVD_VCR            : 
		    {
		     switch(dvd_vcr_mode)
			    {
			     case MODE_IS_VCR: { send_command dvTP,"'@PPN-codec dvd controls'"; }
				    case MODE_IS_DVD: { send_command dvTP,"'@PPN-codec vcr controls'"; }
				    default: {  }
			    }
		    }
				  default: {  }
			  }
				}
				case CODEC_PG_XPORT_CTRLS: { send_command dvTP,"'@PPN-codec call controls'"; }
				default: {  }
			}
		}
		case SRC_PODIUM_1_PC        : { send_command dvTP,"'@PPN-podium'"; }
		case SRC_PODIUM_1_LAPTOP_DVI: { send_command dvTP,"'@PPN-podium'"; }
		case SRC_PODIUM_1_LAPTOP_RGB: { send_command dvTP,"'@PPN-podium'"; }
		case SRC_PODIUM_2_PC        : { send_command dvTP,"'@PPN-podium'"; }
		case SRC_PODIUM_2_LAPTOP_DVI: { send_command dvTP,"'@PPN-podium'"; }
		case SRC_PODIUM_2_LAPTOP_RGB: { send_command dvTP,"'@PPN-podium'"; }
		case SRC_TABLE_1_RGB        : { send_command dvTP,"'@PPN-laptops'"; }
		case SRC_TABLE_2_RGB        : { send_command dvTP,"'@PPN-laptops'"; }
		case SRC_TABLE_3_RGB        : { send_command dvTP,"'@PPN-laptops'"; }
		case SRC_TABLE_4_RGB        : { send_command dvTP,"'@PPN-laptops'"; }
		case SRC_RACK_VGA           : { send_command dvTP,""; }
		case SRC_RACK_SVIDEO        : { send_command dvTP,""; }
		case SRC_DVD_VCR            : 
		{
		 switch(dvd_vcr_mode)
			{
			 case MODE_IS_VCR: { send_command dvTP,"'@PPN-VCR'"; }
				case MODE_IS_DVD: { send_command dvTP,"'@PPN-DVD'"; }
				default: { send_command dvTP,"'@PPN-DVD'"; }
			}
		}
		case SRC_HDDVD              : { send_command dvTP,"'@PPN-BluRay DVD'"; }
		case SRC_SAT                : { send_command dvTP,"'@PPN-sat'"; }
		case SRC_SYSTEM_OFF         : { send_command dvTP,"'@PPN-'"; }
		default: {  }
	}
	// if an options page is active...
 if(tp_options_pg)
	{
	 // pop on options page...
		switch(tp_options_pg)
		{
			case OPTIONS_PG_LIGHTS_SHADES: { send_command dvTP,"'@PPN-lights_shades'"; }
   case OPTIONS_PG_VOL_CTRLS    : { send_command dvTP,"'@PPN-more volume controls'"; }
			default: {  }
		}
	}
}
//-----------------------------------------------//
define_function update_tp_vol_levels()
{
 send_level dvTP,lvlPROG_VOL,vol_level_fb[VOL_PROG];
 send_level dvTP,lvlSPEECH_VOL,vol_level_fb[VOL_SPEECH];
 send_level dvTP,lvlPOD_MIC_VOL,vol_level_fb[VOL_POD_MIC];
 send_level dvTP,lvlWMIC_VOL,vol_level_fb[VOL_WMIC];
}
//=========================================================//
//=========================================================//
DEFINE_START
//---------------------------------------------------------//


//=========================================================//
//=========================================================//
DEFINE_EVENT
//---------------------------------------------------------//
data_event[dvTP]
{
 online:
	{
	
	}
}
//-----------------------------------------------//
// podium location buttons...
button_event[dvTP,btnPOD_LOCATION_1] { push: { podium_location = POD_LOCATION_1; } }
button_event[dvTP,btnPOD_LOCATION_2] { push: { podium_location = POD_LOCATION_2; } }
//-----------------------------------------------//
// refresh page flips button...
button_event[dvTP,btnREFRESH_PG_FLIPS] { push: { do_page_flips(); } }
//-----------------------------------------------//
// source select buttons...
button_event[dvTP,btnSRC_VID_CONF]
button_event[dvTP,btnSRC_PODIUM]  
button_event[dvTP,btnSRC_PODIUM_PC]
button_event[dvTP,btnSRC_PODIUM_LAPTOP_DVI]
button_event[dvTP,btnSRC_PODIUM_LAPTOP_RGB]
button_event[dvTP,btnSRC_TABLE_LAPTOPS] 
button_event[dvTP,btnSRC_TABLE_1_RGB] 
button_event[dvTP,btnSRC_TABLE_2_RGB] 
button_event[dvTP,btnSRC_TABLE_3_RGB] 
button_event[dvTP,btnSRC_TABLE_4_RGB] 
button_event[dvTP,btnSRC_RACK_VGA]    
button_event[dvTP,btnSRC_RACK_SVIDEO] 
button_event[dvTP,btnSRC_DVD_VCR]     
button_event[dvTP,btnSRC_HDDVD]
button_event[dvTP,btnSRC_SAT]
button_event[dvTP,btnSRC_SYSTEM_OFF]   
{
 push:
	{
	 tp_options_pg = 0;
	 switch(button.input.channel)
		{
		 case btnSRC_VID_CONF         : { do_source_select(SRC_VID_CONF, DEST_ROOM); do_source_select(SRC_CAM_1, DEST_VIDEO_CONF); }
		 case btnSRC_PODIUM           : { do_source_select(last_podium_src[podium_location], DEST_ROOM); }
			case btnSRC_PODIUM_PC        : 
			{
			 switch(podium_location)
				{
				 case POD_LOCATION_1:  { do_source_select(SRC_PODIUM_1_PC, DEST_ROOM); }
					case POD_LOCATION_2:  { do_source_select(SRC_PODIUM_2_PC, DEST_ROOM); }
					default: {  }
				}
			}
			case btnSRC_PODIUM_LAPTOP_DVI:
			{
			 switch(podium_location)
				{
				 case POD_LOCATION_1:  { do_source_select(SRC_PODIUM_1_LAPTOP_DVI, DEST_ROOM); }
					case POD_LOCATION_2:  { do_source_select(SRC_PODIUM_2_LAPTOP_DVI, DEST_ROOM); }
					default: {  }
				}
			}
			case btnSRC_PODIUM_LAPTOP_RGB:
			{
			 switch(podium_location)
				{
				 case POD_LOCATION_1:  { do_source_select(SRC_PODIUM_1_LAPTOP_RGB, DEST_ROOM); }
					case POD_LOCATION_2:  { do_source_select(SRC_PODIUM_2_LAPTOP_RGB, DEST_ROOM); }
					default: {  }
				}
			}
			case btnSRC_TABLE_LAPTOPS    : { do_source_select(last_table_laptop, DEST_ROOM); }
		 case btnSRC_TABLE_1_RGB      : { do_source_select(SRC_TABLE_1_RGB, DEST_ROOM); }
		 case btnSRC_TABLE_2_RGB      : { do_source_select(SRC_TABLE_2_RGB, DEST_ROOM); }
			case btnSRC_TABLE_3_RGB      : { do_source_select(SRC_TABLE_3_RGB, DEST_ROOM); }
		 case btnSRC_TABLE_4_RGB      : { do_source_select(SRC_TABLE_4_RGB, DEST_ROOM); }
		 case btnSRC_RACK_VGA         : { do_source_select(SRC_RACK_VGA, DEST_ROOM); }
		 case btnSRC_RACK_SVIDEO      : { do_source_select(SRC_RACK_SVIDEO, DEST_ROOM); }
		 case btnSRC_DVD_VCR          : { do_source_select(SRC_DVD_VCR, DEST_ROOM); }
			case btnSRC_HDDVD            : { do_source_select(SRC_HDDVD, DEST_ROOM); }
			case btnSRC_SAT              : { do_source_select(SRC_SAT, DEST_ROOM); }
		 case btnSRC_SYSTEM_OFF       : { do_system_off(); }
			default: {  }
		}
 }
}
//-----------------------------------------------//
button_event[dvTP,btnOPTIONS_PG_VOL]
button_event[dvTP,btnOPTIONS_PG_LIGHTS_SHADES]
{
 push:
	{
	 switch(button.input.channel)
		{
   case btnOPTIONS_PG_VOL:           
			{
			 if(tp_options_pg == OPTIONS_PG_VOL_CTRLS) { tp_options_pg = 0; }
				else { tp_options_pg = OPTIONS_PG_VOL_CTRLS; }
			}
   case btnOPTIONS_PG_LIGHTS_SHADES: 
			{
    if(tp_options_pg == OPTIONS_PG_LIGHTS_SHADES) { tp_options_pg = 0; }
				else { tp_options_pg = OPTIONS_PG_LIGHTS_SHADES; }
			}
		 default: {  }
		}
		do_page_flips();
	}
}
//-----------------------------------------------//
// volume control buttons...
button_event[dvTP,btnPROG_VOL_UP]
{
 push:           { do_dsp_vol_mute(VOL_PROG, cmdVOL_UP); }
	hold[2,repeat]: { do_dsp_vol_mute(VOL_PROG, cmdVOL_UP); }
}
button_event[dvTP,btnPROG_VOL_DOWN]
{
 push:           { do_dsp_vol_mute(VOL_PROG, cmdVOL_DOWN); }
	hold[2,repeat]: { do_dsp_vol_mute(VOL_PROG, cmdVOL_DOWN); }
}
button_event[dvTP,btnPROG_MUTE]
{
 push: { do_dsp_vol_mute(VOL_PROG, cmdMUTE_TOGGLE); }
}
button_event[dvTP,btnSPPECH_VOL_UP]
{
 push:           { do_dsp_vol_mute(VOL_SPEECH, cmdVOL_UP); }
	hold[2,repeat]: { do_dsp_vol_mute(VOL_SPEECH, cmdVOL_UP); }
}
button_event[dvTP,btnSPEECH_VOL_DOWN]
{
 push:           { do_dsp_vol_mute(VOL_SPEECH, cmdVOL_DOWN); }
	hold[2,repeat]: { do_dsp_vol_mute(VOL_SPEECH, cmdVOL_DOWN); }
}
button_event[dvTP,btnSPEECH_MUTE]
{
 push: { do_dsp_vol_mute(VOL_SPEECH, cmdMUTE_TOGGLE); }
}
button_event[dvTP,btnPODIUM_MIC_VOL_UP]
{
 push:           { do_dsp_vol_mute(VOL_POD_MIC, cmdVOL_UP); }
	hold[2,repeat]: { do_dsp_vol_mute(VOL_POD_MIC, cmdVOL_UP); }
}
button_event[dvTP,btnPODIUM_MIC_VOL_DOWN]
{
 push:           { do_dsp_vol_mute(VOL_POD_MIC, cmdVOL_DOWN); }
	hold[2,repeat]: { do_dsp_vol_mute(VOL_POD_MIC, cmdVOL_DOWN); }
}
button_event[dvTP,btnPODIUM_MIC_MUTE]
{
 push: { do_dsp_vol_mute(VOL_POD_MIC, cmdMUTE_TOGGLE); }
}
button_event[dvTP,btnWMIC_VOL_UP]
{
 push:           { do_dsp_vol_mute(VOL_WMIC, cmdVOL_UP); }
	hold[2,repeat]: { do_dsp_vol_mute(VOL_WMIC, cmdVOL_UP); }
}
button_event[dvTP,btnWMIC_VOL_DOWN]
{
 push:           { do_dsp_vol_mute(VOL_WMIC, cmdVOL_DOWN); }
	hold[2,repeat]: { do_dsp_vol_mute(VOL_WMIC, cmdVOL_DOWN); }
}
button_event[dvTP,btnWMIC_MUTE]
{
 push: { do_dsp_vol_mute(VOL_WMIC, cmdMUTE_TOGGLE); }
}
button_event[dvTP,btnVID_CONF_MUTE_TOGGLE]
{
 push:
	{
	 if(vid_conf_mute) { do_vid_conf_mixer_mute(0); } // if it's already muted, un-mute it.
		else { do_vid_conf_mixer_mute(1); } // otherwise, it's not muted, mute it.
	}
}
//-----------------------------------------------//
// projector blanking buttons...
button_event[dvTP,btnPROJ_MUTE_ON] 
button_event[dvTP,btnPROJ_MUTE_OFF]
{
 push:
	{
	 switch(button.input.channel)
		{
		 //case btnPROJ_MUTE_ON : { do_proj_cmd(cmdMUTE_ON); }
   //case btnPROJ_MUTE_OFF: { do_proj_cmd(cmdMUTE_OFF); }
			default: {  }
		}
	}
}
//-----------------------------------------------//
// codec page buttons...
button_event[dvTP,btnCODEC_PG_SRC_CTRLS]
button_event[dvTP,btnCODEC_PG_XPORT_CTRLS]
{
 push:
	{
	 switch(button.input.channel)
		{
		 case btnCODEC_PG_SRC_CTRLS:   { tp_codec_pg = CODEC_PG_SRC_CTRLS; }
   case btnCODEC_PG_XPORT_CTRLS: { tp_codec_pg = CODEC_PG_XPORT_CTRLS; }
			default: {  }
		}
		do_page_flips();
	}
}
//-----------------------------------------------//
// codec source select buttons...
button_event[dvTP,btnCODEC_SRC_PODIUM_LAPTOP]  
button_event[dvTP,btnCODEC_SRC_PODIUM_PC]  
button_event[dvTP,btnCODEC_SRC_TABLE_1_RGB] 
button_event[dvTP,btnCODEC_SRC_TABLE_2_RGB] 
button_event[dvTP,btnCODEC_SRC_TABLE_3_RGB] 
button_event[dvTP,btnCODEC_SRC_TABLE_4_RGB] 
button_event[dvTP,btnCODEC_SRC_RACK_VGA]    
button_event[dvTP,btnCODEC_SRC_RACK_SVIDEO] 
button_event[dvTP,btnCODEC_SRC_DVD_VCR]     
button_event[dvTP,btnCODEC_SRC_CAM_1]       
button_event[dvTP,btnCODEC_SRC_CAM_2]       
{
 push:
	{
	 switch(button.input.channel)
		{
		 case btnCODEC_SRC_PODIUM_PC  : 
			{
			 switch(podium_location)
				{
				 case POD_LOCATION_1: { do_source_select(SRC_PODIUM_1_PC, DEST_VIDEO_CONF);  }
					case POD_LOCATION_2: { do_source_select(SRC_PODIUM_2_PC, DEST_VIDEO_CONF);  }
					default: {  }
				}
			}
			case btnCODEC_SRC_PODIUM_LAPTOP  : 
			{
			 switch(podium_location)
				{
				 case POD_LOCATION_1: { do_source_select(SRC_PODIUM_1_LAPTOP_RGB, DEST_VIDEO_CONF);  }
					case POD_LOCATION_2: { do_source_select(SRC_PODIUM_2_LAPTOP_RGB, DEST_VIDEO_CONF);  }
					default: {  }
				}
			}
		 case btnCODEC_SRC_TABLE_1_RGB: { do_source_select(SRC_TABLE_1_RGB, DEST_VIDEO_CONF); }
		 case btnCODEC_SRC_TABLE_2_RGB: { do_source_select(SRC_TABLE_2_RGB, DEST_VIDEO_CONF); }
			case btnCODEC_SRC_TABLE_3_RGB: { do_source_select(SRC_TABLE_3_RGB, DEST_VIDEO_CONF); }
		 case btnCODEC_SRC_TABLE_4_RGB: { do_source_select(SRC_TABLE_4_RGB, DEST_VIDEO_CONF); }
		 case btnCODEC_SRC_RACK_VGA   : { do_source_select(SRC_RACK_VGA, DEST_VIDEO_CONF); }
		 case btnCODEC_SRC_RACK_SVIDEO: { do_source_select(SRC_RACK_SVIDEO, DEST_VIDEO_CONF); }
		 case btnCODEC_SRC_DVD_VCR    : { do_source_select(SRC_DVD_VCR, DEST_VIDEO_CONF); }
		 case btnCODEC_SRC_CAM_1      : { do_source_select(SRC_CAM_1, DEST_VIDEO_CONF); }
		 case btnCODEC_SRC_CAM_2      : { do_source_select(SRC_CAM_2, DEST_VIDEO_CONF); }
			default: {  }
  }
 }
}
//-----------------------------------------------//
// codec control buttons...
button_event[dvTP,btnCODEC_CALL]      
button_event[dvTP,btnCODEC_TRIANGLE]  
button_event[dvTP,btnCODEC_SQUARE]    
button_event[dvTP,btnCODEC_CIRCLE]    
button_event[dvTP,btnCODEC_BACK]      
button_event[dvTP,btnCODEC_OK]        
button_event[dvTP,btnCODEC_CUR_UP]    
button_event[dvTP,btnCODEC_CUR_DOWN]  
button_event[dvTP,btnCODEC_CUR_LEFT]  
button_event[dvTP,btnCODEC_CUR_RIGHT] 
button_event[dvTP,btnCODEC_VOL_UP]    
button_event[dvTP,btnCODEC_VOL_DOWN]  
button_event[dvTP,btnCODEC_MUTE]      
//button_event[dvTP,btnCODEC_ZOOM_IN]  
//button_event[dvTP,btnCODEC_ZOOM_OUT]  
button_event[dvTP,btnCODEC_NEAR]      
button_event[dvTP,btnCODEC_FAR]       
button_event[dvTP,btnCODEC_0]         
button_event[dvTP,btnCODEC_1]         
button_event[dvTP,btnCODEC_2]         
button_event[dvTP,btnCODEC_3]         
button_event[dvTP,btnCODEC_4]         
button_event[dvTP,btnCODEC_5]         
button_event[dvTP,btnCODEC_6]         
button_event[dvTP,btnCODEC_7]         
button_event[dvTP,btnCODEC_8]         
button_event[dvTP,btnCODEC_9]         
button_event[dvTP,btnCODEC_POUND]     
button_event[dvTP,btnCODEC_STAR]    
button_event[dvTP,btnCODEC_HANGUP]
{
 push:
	{
  switch(button.input.channel)
		{
   case btnCODEC_CALL      : { do_codec_cmd(CODEC_CMD_CALL); }
   case btnCODEC_TRIANGLE  : { do_codec_cmd(CODEC_CMD_TRIANGLE); }
   case btnCODEC_SQUARE    : { do_codec_cmd(CODEC_CMD_SQUARE); }
   case btnCODEC_CIRCLE    : { do_codec_cmd(CODEC_CMD_CIRCLE); }
   case btnCODEC_BACK      : { do_codec_cmd(CODEC_CMD_BACK); }
   case btnCODEC_OK        : { do_codec_cmd(CODEC_CMD_OK); }
   case btnCODEC_CUR_UP    : { do_codec_cmd(CODEC_CMD_CUR_UP); }
   case btnCODEC_CUR_DOWN  : { do_codec_cmd(CODEC_CMD_CUR_DOWN); }
   case btnCODEC_CUR_LEFT  : { do_codec_cmd(CODEC_CMD_CUR_LEFT); }
   case btnCODEC_CUR_RIGHT : { do_codec_cmd(CODEC_CMD_CUR_RIGHT); }
   case btnCODEC_VOL_UP    : { do_codec_cmd(CODEC_CMD_VOL_UP); }
   case btnCODEC_VOL_DOWN  : { do_codec_cmd(CODEC_CMD_VOL_DOWN); }
   case btnCODEC_MUTE      : { do_codec_cmd(CODEC_CMD_MUTE); }
 //  case btnCODEC_ZOOM_IN   : { do_codec_cmd(CODEC_CMD_ZOOM_IN); }
//   case btnCODEC_ZOOM_OUT  : { do_codec_cmd(CODEC_CMD_ZOOM_OUT); }
   case btnCODEC_NEAR      : { do_codec_cmd(CODEC_CMD_NEAR); }
   case btnCODEC_FAR       : { do_codec_cmd(CODEC_CMD_FAR); }
   case btnCODEC_0         : { do_codec_cmd(CODEC_CMD_0); }
   case btnCODEC_1         : { do_codec_cmd(CODEC_CMD_1); }
   case btnCODEC_2         : { do_codec_cmd(CODEC_CMD_2); }
   case btnCODEC_3         : { do_codec_cmd(CODEC_CMD_3); }
   case btnCODEC_4         : { do_codec_cmd(CODEC_CMD_4); }
   case btnCODEC_5         : { do_codec_cmd(CODEC_CMD_5); }
   case btnCODEC_6         : { do_codec_cmd(CODEC_CMD_6); }
   case btnCODEC_7         : { do_codec_cmd(CODEC_CMD_7); }
   case btnCODEC_8         : { do_codec_cmd(CODEC_CMD_8); }
   case btnCODEC_9         : { do_codec_cmd(CODEC_CMD_9); }
   case btnCODEC_POUND     : { do_codec_cmd(CODEC_CMD_POUND); }
   case btnCODEC_STAR      : { do_codec_cmd(CODEC_CMD_STAR); }
			case btnCODEC_HANGUP    : { codec_hangup_call(); }
			default: {  }
  }
 }
}
//-----------------------------------------------//
// codec camera control buttons...
button_event[dvTP,btnCODEC_CAM_UP]       
{
 push:    { do_codec_cam_ctrl(cmdCUR_UP); }
	release: { do_codec_cam_ctrl(cmdSTOP); }
}
button_event[dvTP,btnCODEC_CAM_DOWN]     
{
 push:    { do_codec_cam_ctrl(cmdCUR_DOWN); }
	release: { do_codec_cam_ctrl(cmdSTOP); }
}
button_event[dvTP,btnCODEC_CAM_LEFT]     
{
 push:    { do_codec_cam_ctrl(cmdCUR_LEFT); }
	release: { do_codec_cam_ctrl(cmdSTOP); }
}
button_event[dvTP,btnCODEC_CAM_RIGHT]    
{
 push:    { do_codec_cam_ctrl(cmdCUR_RIGHT); }
	release: { do_codec_cam_ctrl(cmdSTOP); }
}
button_event[dvTP,btnCODEC_CAM_ZOOM_IN]  
{
 push:    { do_codec_cam_ctrl(cmdZOOM_IN); }
	release: { do_codec_cam_ctrl(cmdSTOP); }
}
button_event[dvTP,btnCODEC_CAM_ZOOM_OUT] 
{
 push:    { do_codec_cam_ctrl(cmdZOOM_OUT); }
	release: { do_codec_cam_ctrl(cmdSTOP); }
}
//-----------------------------------------------//
// dvd / vcr transport buttons...
button_event[dvTP,btnPLAY]
button_event[dvTP,btnSTOP]
button_event[dvTP,btnPAUSE]
button_event[dvTP,btnTRACK_FWD]
button_event[dvTP,btnTRACK_BACK]
button_event[dvTP,btnFFWD]
button_event[dvTP,btnREW]
button_event[dvTP,btnMENU]
button_event[dvTP,btnCUR_UP]
button_event[dvTP,btnCUR_DOWN]
button_event[dvTP,btnCUR_LEFT]
button_event[dvTP,btnCUR_RIGHT]
button_event[dvTP,btnENTER]
button_event[dvTP,btnSUBTITLE]
button_event[dvTP,btnRETURN]
button_event[dvTP,btnDVD_VCR]
button_event[dvTP,btnPG_UP]      
button_event[dvTP,btnPG_DOWN]    
button_event[dvTP,btnGUIDE]      
button_event[dvTP,btnRECALL]     
button_event[dvTP,btnCANCEL]     
button_event[dvTP,btnLIVE_TV]    
button_event[dvTP,btnDVR]        
button_event[dvTP,btnRECORD]     
button_event[dvTP,btnNUM_1]      
button_event[dvTP,btnNUM_2]      
button_event[dvTP,btnNUM_3]      
button_event[dvTP,btnNUM_4]      
button_event[dvTP,btnNUM_5]      
button_event[dvTP,btnNUM_6]      
button_event[dvTP,btnNUM_7]      
button_event[dvTP,btnNUM_8]      
button_event[dvTP,btnNUM_9]      
button_event[dvTP,btnNUM_0]      
button_event[dvTP,btnSTAR]       
button_event[dvTP,btnPOUND]
button_event[dvTP,btnRED]
button_event[dvTP,btnGREEN]
button_event[dvTP,btnBLUE]
button_event[dvTP,btnYELLOW]
{
 push:
	{
	 switch(button.input.channel)
		{
		 case btnDVD_VCR   : { do_xport(current_src[DEST_ROOM], cmdDVD_VCR); }
		 case btnPLAY      : { do_xport(current_src[DEST_ROOM], cmdPLAY); }
		 case btnSTOP      : { do_xport(current_src[DEST_ROOM], cmdSTOP); }
		 case btnPAUSE     : { do_xport(current_src[DEST_ROOM], cmdPAUSE); }
		 case btnTRACK_FWD : { do_xport(current_src[DEST_ROOM], cmdTRACK_FWD); }
		 case btnTRACK_BACK: { do_xport(current_src[DEST_ROOM], cmdTRACK_BACK); }
		 case btnFFWD      : { do_xport(current_src[DEST_ROOM], cmdFFWD); }
		 case btnREW       : { do_xport(current_src[DEST_ROOM], cmdREW); }
		 case btnMENU      : { do_xport(current_src[DEST_ROOM], cmdMENU); }
		 case btnCUR_UP    : { do_xport(current_src[DEST_ROOM], cmdCUR_UP); }
		 case btnCUR_DOWN  : { do_xport(current_src[DEST_ROOM], cmdCUR_DOWN); }
		 case btnCUR_LEFT  : { do_xport(current_src[DEST_ROOM], cmdCUR_LEFT); }
		 case btnCUR_RIGHT : { do_xport(current_src[DEST_ROOM], cmdCUR_RIGHT); }
		 case btnENTER     : { do_xport(current_src[DEST_ROOM], cmdSELECT); }
		 case btnSUBTITLE  : { do_xport(current_src[DEST_ROOM], cmdSUBTITLE); }
		 case btnRETURN    : { do_xport(current_src[DEST_ROOM], cmdRETURN); }
		 case btnPG_UP     : { do_xport(current_src[DEST_ROOM], cmdPAGE_UP); }
		 case btnPG_DOWN   : { do_xport(current_src[DEST_ROOM], cmdPAGE_DOWN); }
		 case btnGUIDE     : { do_xport(current_src[DEST_ROOM], cmdGUIDE); }
		 case btnRECALL    : { do_xport(current_src[DEST_ROOM], cmdRECALL); }
		 case btnCANCEL    : { do_xport(current_src[DEST_ROOM], cmdCANCEL); }
		 case btnLIVE_TV   : { do_xport(current_src[DEST_ROOM], cmdLIVE_TV); }
		 case btnDVR       : { do_xport(current_src[DEST_ROOM], cmdDVR); }
		 case btnRECORD    : { do_xport(current_src[DEST_ROOM], cmdRECORD); }
		 case btnNUM_1     : { do_xport(current_src[DEST_ROOM], cmdNUM_1); }
		 case btnNUM_2     : { do_xport(current_src[DEST_ROOM], cmdNUM_2); }
		 case btnNUM_3     : { do_xport(current_src[DEST_ROOM], cmdNUM_3); }
		 case btnNUM_4     : { do_xport(current_src[DEST_ROOM], cmdNUM_4); }
		 case btnNUM_5     : { do_xport(current_src[DEST_ROOM], cmdNUM_5); }
		 case btnNUM_6     : { do_xport(current_src[DEST_ROOM], cmdNUM_6); }
		 case btnNUM_7     : { do_xport(current_src[DEST_ROOM], cmdNUM_7); }
		 case btnNUM_8     : { do_xport(current_src[DEST_ROOM], cmdNUM_8); }
		 case btnNUM_9     : { do_xport(current_src[DEST_ROOM], cmdNUM_9); }
		 case btnNUM_0     : { do_xport(current_src[DEST_ROOM], cmdNUM_0); }
		 case btnSTAR      : { do_xport(current_src[DEST_ROOM], cmdSTAR); }
		 case btnPOUND     : { do_xport(current_src[DEST_ROOM], cmdPOUND); }
		 case btnRED       : { do_xport(current_src[DEST_ROOM], cmdRED); }
		 case btnGREEN     : { do_xport(current_src[DEST_ROOM], cmdGREEN); }
		 case btnBLUE      : { do_xport(current_src[DEST_ROOM], cmdBLUE); }
		 case btnYELLOW    : { do_xport(current_src[DEST_ROOM], cmdYELLOW); }
   default: {  }
  }
	}
}
button_event[dvTP,btnEXT_CUR_UP]
button_event[dvTP,btnEXT_CUR_DOWN]
button_event[dvTP,btnEXT_CUR_LEFT]
button_event[dvTP,btnEXT_CUR_RIGHT]
button_event[dvTP,btnEXT_ENTER]
{
 push:
 {
  switch(button.input.channel)
	{
	 case btnEXT_CUR_UP:    { do_xport(current_src[DEST_ROOM], cmdCUR_UP); }
	 case btnEXT_CUR_DOWN:  { do_xport(current_src[DEST_ROOM], cmdCUR_DOWN); }
	 case btnEXT_CUR_LEFT:  { do_xport(current_src[DEST_ROOM], cmdCUR_LEFT); }
	 case btnEXT_CUR_RIGHT: { do_xport(current_src[DEST_ROOM], cmdCUR_RIGHT); }
	 case btnEXT_ENTER:     { do_xport(current_src[DEST_ROOM], cmdSELECT); }
   default: {  }
	}
 } 
}
//-----------------------------------------------//
// lighting buttons...
button_event[dvTP,btnLIGHTS_OFF]      
button_event[dvTP,btnLIGHTS_PRESET_1] 
button_event[dvTP,btnLIGHTS_PRESET_2] 
button_event[dvTP,btnLIGHTS_PRESET_3] 
button_event[dvTP,btnLIGHTS_PRESET_4] 
{
 push:
	{
  switch(button.input.channel)
	 {
   case btnLIGHTS_OFF     : { lights_recall_preset(0); do_shades(cmdSHADES_CLOSE); }
   case btnLIGHTS_PRESET_1: { lights_recall_preset(1); do_shades(cmdSHADES_OPEN); }
   case btnLIGHTS_PRESET_2: { lights_recall_preset(2); do_shades(cmdSHADES_OPEN); }
   case btnLIGHTS_PRESET_3: { lights_recall_preset(3); do_shades(cmdSHADES_MIDDLE); }
   case btnLIGHTS_PRESET_4: { lights_recall_preset(4); do_shades(cmdSHADES_MIDDLE); }
		 default: {  }
	 }
	}
}
//-----------------------------------------------//
// shades buttons...
button_event[dvTP,btnSHADES_OPEN]
button_event[dvTP,btnSHADES_CLOSE]
button_event[dvTP,btnSHADES_MIDDLE]
{
 push:
	{
	 switch(button.input.channel)
		{
			case btnSHADES_OPEN:   { do_shades(cmdSHADES_OPEN); }
			case btnSHADES_CLOSE:  { do_shades(cmdSHADES_CLOSE); }
			case btnSHADES_MIDDLE: { do_shades(cmdSHADES_MIDDLE); }
			default: {  }
		}
	}
}
//-----------------------------------------------//
// screen buttons...
button_event[dvTP,btnSCREEN_UP]
button_event[dvTP,btnSCREEN_DOWN]
{
 push:
	{
	 switch(button.input.channel)
		{
			case btnSCREEN_UP:     { do_screen(cmdCUR_UP); }
			case btnSCREEN_DOWN:   { do_screen(cmdCUR_DOWN); }
			default: {  }
		}
	}
}
//=========================================================//
//=========================================================//
DEFINE_PROGRAM
//---------------------------------------------------------//
// make flash flash
wait 5 { flash = !flash; }
//-----------------------------------------------//
// podium location feedback....
[dvTP,btnPOD_LOCATION_1] = ( podium_location == POD_LOCATION_1 );
[dvTP,btnPOD_LOCATION_2] = ( podium_location == POD_LOCATION_2 );
//-----------------------------------------------//
// source button feedback...
[dvTP,btnSRC_VID_CONF]      = ( current_src[DEST_ROOM] == SRC_VID_CONF );
[dvTP,btnSRC_PODIUM]        = ( current_src[DEST_ROOM] == SRC_PODIUM_1_PC ||
                                current_src[DEST_ROOM] == SRC_PODIUM_1_LAPTOP_DVI ||
																																current_src[DEST_ROOM] == SRC_PODIUM_1_LAPTOP_RGB ||
																																current_src[DEST_ROOM] == SRC_PODIUM_2_PC ||
                                current_src[DEST_ROOM] == SRC_PODIUM_2_LAPTOP_DVI ||
																																current_src[DEST_ROOM] == SRC_PODIUM_2_LAPTOP_RGB	);
[dvTP,btnSRC_PODIUM_PC]     = ( current_src[DEST_ROOM] == SRC_PODIUM_1_PC ||
																																current_src[DEST_ROOM] == SRC_PODIUM_2_PC );
[dvTP,btnSRC_PODIUM_LAPTOP_DVI]= ( current_src[DEST_ROOM] == SRC_PODIUM_1_LAPTOP_DVI ||
																																   current_src[DEST_ROOM] == SRC_PODIUM_2_LAPTOP_DVI );
[dvTP,btnSRC_PODIUM_LAPTOP_RGB]= ( current_src[DEST_ROOM] == SRC_PODIUM_1_LAPTOP_RGB ||
																																   current_src[DEST_ROOM] == SRC_PODIUM_2_LAPTOP_RGB );
[dvTP,btnSRC_TABLE_LAPTOPS] = ( current_src[DEST_ROOM] == SRC_TABLE_1_RGB ||
																																current_src[DEST_ROOM] == SRC_TABLE_2_RGB ||
																																current_src[DEST_ROOM] == SRC_TABLE_3_RGB ||
																																current_src[DEST_ROOM] == SRC_TABLE_4_RGB );
[dvTP,btnSRC_TABLE_1_RGB]   = ( current_src[DEST_ROOM] == SRC_TABLE_1_RGB );
[dvTP,btnSRC_TABLE_2_RGB]   = ( current_src[DEST_ROOM] == SRC_TABLE_2_RGB );
[dvTP,btnSRC_TABLE_3_RGB]   = ( current_src[DEST_ROOM] == SRC_TABLE_3_RGB );
[dvTP,btnSRC_TABLE_4_RGB]   = ( current_src[DEST_ROOM] == SRC_TABLE_4_RGB );
[dvTP,btnSRC_RACK_VGA]      = ( current_src[DEST_ROOM] == SRC_RACK_VGA );
[dvTP,btnSRC_RACK_SVIDEO]   = ( current_src[DEST_ROOM] == SRC_RACK_SVIDEO );
[dvTP,btnSRC_DVD_VCR]       = ( current_src[DEST_ROOM] == SRC_DVD_VCR );
[dvTP,btnSRC_HDDVD]         = ( current_src[DEST_ROOM] == SRC_HDDVD );
[dvTP,btnSRC_SAT]           = ( current_src[DEST_ROOM] == SRC_SAT );
[dvTP,btnSRC_SYSTEM_OFF]    = ( current_src[DEST_ROOM] == SRC_SYSTEM_OFF );
[dvTP,btnSYSTEM_OFF]        = ( current_src[DEST_ROOM] == SRC_SYSTEM_OFF );
//-----------------------------------------------//
// mute button feedback...
[dvTP,btnPROG_MUTE]       = ( mute_fb[VOL_PROG] == 1 && flash );
[dvTP,btnSPEECH_MUTE]     = ( mute_fb[VOL_SPEECH] == 1 && flash );
[dvTP,btnPODIUM_MIC_MUTE] = ( mute_fb[VOL_POD_MIC] == 1 && flash );
[dvTP,btnWMIC_MUTE]       = ( mute_fb[VOL_WMIC] == 1 && flash );
[dvTP,btnVID_CONF_MUTE_TOGGLE] = ( vid_conf_mute == 1 && flash );
//-----------------------------------------------//
// projector blank buttons feedback...
//[dvTP,btnPROJ_MUTE_ON]  = [vdvPROJ,chPROJ_PIC_MUTE_ON];
//[dvTP,btnPROJ_MUTE_OFF] = [vdvPROJ,chPROJ_PIC_MUTE_OFF];
//-----------------------------------------------//
// codec page buttons feedback...
[dvTP,btnCODEC_PG_SRC_CTRLS]   = ( tp_codec_pg == CODEC_PG_SRC_CTRLS );
[dvTP,btnCODEC_PG_XPORT_CTRLS] = ( tp_codec_pg == CODEC_PG_XPORT_CTRLS );
//-----------------------------------------------//
// codec source buttons feedback...
[dvTP,btnCODEC_SRC_PODIUM_PC]   = ( current_src[DEST_VIDEO_CONF] == SRC_PODIUM_1_PC ||
                                    current_src[DEST_VIDEO_CONF] == SRC_PODIUM_2_PC );
[dvTP,btnCODEC_SRC_PODIUM_LAPTOP]= ( current_src[DEST_VIDEO_CONF] == SRC_PODIUM_1_LAPTOP_RGB ||
                                     current_src[DEST_VIDEO_CONF] == SRC_PODIUM_2_LAPTOP_RGB );
[dvTP,btnCODEC_SRC_TABLE_1_RGB] = ( current_src[DEST_VIDEO_CONF] == SRC_TABLE_1_RGB );
[dvTP,btnCODEC_SRC_TABLE_2_RGB] = ( current_src[DEST_VIDEO_CONF] == SRC_TABLE_2_RGB );
[dvTP,btnCODEC_SRC_TABLE_3_RGB] = ( current_src[DEST_VIDEO_CONF] == SRC_TABLE_3_RGB );
[dvTP,btnCODEC_SRC_TABLE_4_RGB] = ( current_src[DEST_VIDEO_CONF] == SRC_TABLE_4_RGB );
[dvTP,btnCODEC_SRC_RACK_VGA]    = ( current_src[DEST_VIDEO_CONF] == SRC_RACK_VGA    );
[dvTP,btnCODEC_SRC_RACK_SVIDEO] = ( current_src[DEST_VIDEO_CONF] == SRC_RACK_SVIDEO );
[dvTP,btnCODEC_SRC_DVD_VCR]     = ( current_src[DEST_VIDEO_CONF] == SRC_DVD_VCR     );
[dvTP,btnCODEC_SRC_CAM_1]       = ( current_src[DEST_VIDEO_CONF] == SRC_CAM_1       );
[dvTP,btnCODEC_SRC_CAM_2]       = ( current_src[DEST_VIDEO_CONF] == SRC_CAM_2       );
//-----------------------------------------------//
// lighting buttons feedback...
[dvTP,btnLIGHTS_OFF]      = ( lutron_scene_fb[1] == 0 );
[dvTP,btnLIGHTS_PRESET_1] = ( lutron_scene_fb[1] == 1 );
[dvTP,btnLIGHTS_PRESET_2] = ( lutron_scene_fb[1] == 2 );
[dvTP,btnLIGHTS_PRESET_3] = ( lutron_scene_fb[1] == 3 );
[dvTP,btnLIGHTS_PRESET_4] = ( lutron_scene_fb[1] == 4 );
//=========================================================//
//                     END OF PROGRAM                      //
//        DO NOT PUT ANY CODE BELOW THIS COMMENT           //
//=========================================================//